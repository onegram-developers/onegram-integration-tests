import threading
from utils import threading_helpers
import subprocess
import sys
import time
import os


# TODO: make this 'line_processor' ???
class ObserveMemory(threading.Thread):
    def __init__(self, pid, sampling=1):
        self.pid = pid
        self.max = 0
        self.min = sys.maxsize
        self.avg = 0
        self.count = 0
        self.is_running = True
        self.sampling = sampling
        super().__init__()

    def run(self):
        while self.is_running:
            time.sleep(self.sampling)
            private, shared = threading_helpers.get_mem_stats(self.pid)
            mem = private + shared

            if mem == 0:
                break

            self.max = max(mem, self.max)
            self.min = min(mem, self.min)
            self.avg += mem
            self.count += 1

        if self.count != 0:
            self.avg = self.avg / self.count

        # TODO: write to output file (stream)
        print("Memory Statistics: \n min: {}\n max: {}\n avg: {}\n".format(self.min, self.max, self.avg))

    def stop(self):
        self.is_running = False


class Node:
    class NodeThread(threading.Thread):

        def __init__(self, n, c):
            self.node = n
            self.context = c
            super().__init__()

        @threading_helpers.catch_exception
        def run(self):
            result = self.node.run(self.context)
            assert result == 0
            self.context.finished = True

    def __init__(self, executable):
        self.executable = executable

    def run_session(self, param_list, output_processors, container=None):
        context = threading_helpers.Context()
        context.params = param_list
        context.processors = output_processors
        context.container = container

        if container:
            self.run(context)
        else:
            t = self.NodeThread(self, context)
            t.start()

            while not context.finished:
                if context.exception:  # exception found
                    for thread in context.threads:
                        threading_helpers.terminate_thread(thread)
                    raise context.exception

    def run(self, context):
        return_value = -1
        return_msg = ""

        node_process = None
        mem_observer = None
        old_mode = None

        try:
            start_time = time.time()

            node_start = [self.executable] + context.params

            print("\nExecution params: " + ' '.join(node_start))

            container = context.container
            output_iter = None

            if container is None:
                old_mode = os.stat(self.executable).st_mode & 0o777
                os.chmod(self.executable, 0o777)
                node_process = subprocess.Popen(node_start, stderr=subprocess.PIPE)

                mem_observer = ObserveMemory(node_process.pid)
                mem_observer.start()

                output_iter = iter(node_process.stderr.readline, b'')
            else:
                assert (container.status == 'created'), "We expect container to be up"

                # TODO: use docker stats in mem observing thread
                command = ' '.join(node_start)
                r = container.exec_run(cmd=command,
                                       user='root',
                                       tty=False,
                                       stream=True,
                                       detach=False)

                output_iter = r.output

            for processor in context.processors:
                processor.initialize(context)

            for line in output_iter:
                line_str = line.decode("utf-8")

                break_loop = False
                for processor in context.processors:
                    res = processor.process_line(line_str)

                    if res.break_loop:
                        return_msg = res.return_msg
                        return_value = res.return_value
                        break_loop = True

                if break_loop:
                    break
            else:
                if container:
                    print(f"container status: {container.status}")

                raise RuntimeError("No output detected!")

        except Exception as e:
            print(f'{e}')
            return_value = -1
            return_msg = "Something went horribly wrong!"
        finally:
            print("Finalizing processors")
            for processor in context.processors:
                processor.finalize()

            if node_process and threading_helpers.check_pid(node_process.pid):
                print("Killing process")
                node_process.kill()

            if mem_observer:
                mem_observer.stop()

            if old_mode is not None:
                os.chmod(self.executable, old_mode)

            end_time = time.time()

            ellaped_time = end_time - start_time
            print(ellaped_time)  # TODO: write to output file (stream)

            # TODO: compare output file with reference file

            print(return_msg)
            return return_value
