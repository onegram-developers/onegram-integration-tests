import threading
import os
import ctypes


class Context:
    def __init__(self):
        self.exception = None
        self.finished = False
        self.threads = []


def check_pid(pid):
    try:
        os.kill(pid, 0)
    except OSError:
        return False
    else:
        return True


def find_thread_id(thread):
    if thread.is_alive():
        for t_id, t_obj in threading._active.items():
            if t_obj is thread:
                return t_id
    return None


def async_raise(tid, exctype):
    """raises the exception, performs cleanup if needed"""
    res = ctypes.pythonapi.PyThreadState_SetAsyncExc(
        ctypes.c_long(tid),
        ctypes.py_object(exctype)
    )
    if res == 0:
        raise ValueError("invalid thread id")
    elif res != 1:
        ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, 0)
        raise SystemError("PyThreadState_SetAsyncExc failed")


def terminate_thread(thread):
    tid = find_thread_id(thread)
    if tid:
        async_raise(tid, SystemExit)


def catch_exception(func):

    def wrapper(self):
        self.context.threads.append(self)
        try:
            func(self)
        except Exception as e:
            print(e)
            self.context.exception = e

    return wrapper


# TODO: should we use 'resource' package: https://docs.python.org/3/library/resource.html ?
def get_mem_stats(pid):
    if not check_pid(pid):
        print("Process %d not running" % pid)
        return 0, 0

    uname = os.uname()
    if uname[0] == "FreeBSD":
        proc = "/compat/linux/proc/"
    else:
        proc = "/proc/"

    page_size = os.sysconf("SC_PAGE_SIZE") / 1024  # KiB

    statm_path = proc + str(pid) + "/statm"
    smaps_path = proc + str(pid) + "/smaps"

    private_lines = []
    shared_lines = []
    pss_lines = []
    rss = 0
    if os.path.exists(statm_path):
        rss = int(open(statm_path, "rt").readline().split()[1])
        rss *= page_size

    if os.path.exists(smaps_path):
        for line in open(smaps_path, "rb").readlines():
            line = line.decode("ascii")
            if line.startswith("Shared"):
                shared_lines.append(line)
            elif line.startswith("Private"):
                private_lines.append(line)
            elif line.startswith("Pss"):
                pss_lines.append(line)
        shared = sum([int(line.split()[1]) for line in shared_lines])
        private = sum([int(line.split()[1]) for line in private_lines])
        if len(pss_lines) > 0:
            pss_adjust = 0.5  # add 0.5KiB as this avg error due to trunctation
            pss = sum([float(line.split()[1]) + pss_adjust for line in pss_lines])
            shared = pss - private
    else:
        shared = 0
        if os.path.exists(statm_path):
            shared = int(open(statm_path, "rt").readline().split()[2])
            shared *= page_size
        private = rss - shared

    return private, shared

