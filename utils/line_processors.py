import re
import time
import threading
from utils import threading_helpers


# TODO: should we rename this to something more meaningful ?
class ProcessorResult:
    def __init__(self, do_break=False, msg="", value=0):
        self.break_loop = do_break
        self.return_msg = msg
        self.return_value = value


class RegExOutputFilter:
    """ Takes list of tuples or strings as an input.
        Tuple represents regexp & value if line should match or not this regexp.
        By default it should match regexp.
    """
    def __init__(self, *regex_strings):
        self.expressions = []
        for exp_string in regex_strings:
            exp_str = exp_string
            exp_val = True
            if type(exp_string) is tuple:
                exp_str = exp_string[0]
                exp_val = exp_string[1]

            exp = (re.compile(exp_str), exp_val)
            self.expressions.append(exp)

    def can_pass(self, line):
        for exp in self.expressions:
            result = exp[0].search(line)
            if (result is None) == exp[1]:
                return False
        return True


class Write2StreamProcessor:
    """This 'processor' writes every processed line to given stream"""
    def __init__(self, stream, o_filter=RegExOutputFilter()):
        self.stream = stream
        self.o_filter = o_filter

    def initialize(self, context):
        pass

    def process_line(self, line):
        if self.o_filter.can_pass(line):
            self.stream.write(line)
        return ProcessorResult()

    def finalize(self):
        self.stream.flush()


class ExpectingOutputProcessor:
    """This 'processor' terminates processing (raise exception), when time between two 'lines' is too big."""
    class WatchingThread(threading.Thread):
        def __init__(self, parent):
            self.parent = parent
            self.context = None
            self.is_running = True
            super().__init__()

        @threading_helpers.catch_exception
        def run(self):
            while self.is_running:
                time.sleep(self.parent.time_interval)
                now = time.time()
                interval = now - self.parent.checkpoint
                if interval > self.parent.time_interval:
                    raise Exception("No output in time %f" % interval)

    def __init__(self, max_time_interval_sec):
        self.time_interval = max_time_interval_sec
        self.checkpoint = time.time()
        self.watchdog = self.WatchingThread(self)

    def initialize(self, context):
        self.checkpoint = time.time()
        self.watchdog.context = context
        self.watchdog.start()
        print("Expecting output every %d seconds" % self.time_interval)

    def process_line(self, line):
        self.checkpoint = time.time()
        return ProcessorResult()

    def finalize(self):
        self.watchdog.is_running = False


class BlockSyncProcessor:
    """This 'processor' waits for blocks to be synced."""
    def __init__(self, expected_block=None, do_break=True, sampling_step=5000):
        self.block_got_line_regexp = re.compile("#([0-9]+)")
        self.block_sync_line_regexp = re.compile("Successfully pushed sync block ([0-9]+)")
        self.expected_block = expected_block
        self.break_on_expected_block = do_break
        self.prev_block = 0
        self.sampling_step = sampling_step

    def initialize(self, context):
        if self.expected_block is not None:
            print("Waiting for block %d" % self.expected_block)

    def process_line(self, line):
        block_num = -1
        if "Got block" in line:
            m = self.block_got_line_regexp.search(line)
            block_num_str = m.group(1)
            block_num = int(block_num_str)
            assert self.prev_block <= block_num  # this assert is valid only for 'Got block'
        if "Successfully pushed sync block" in line:
            m = self.block_sync_line_regexp.search(line)
            block_num_str = m.group(1)
            block_num = int(block_num_str)

        if block_num > 0:
            if (block_num != self.prev_block) and (block_num % self.sampling_step == 0):
                print("Latest block number %d" % block_num)

            self.prev_block = block_num

            synced_with_last_block = ((self.expected_block is not None) and (block_num >= self.expected_block)) \
                                     or ((self.expected_block is None) and (block_num % 10 > 0))

            if synced_with_last_block:
                return ProcessorResult(
                    self.break_on_expected_block,
                    "Blocks successfully synced!",
                )

        return ProcessorResult()

    def finalize(self):
        pass


class ReindexingProcessor:
    """This 'processor' waits for database to be reindexed."""
    def __init__(self, do_break=True):
        self.replaying_regexp = re.compile("\s*([0-9]+\.[0-9]+%)\s*([0-9]+) of ([0-9]+)\s*")
        self.break_on_finish = do_break

    def initialize(self, context):
        pass

    def process_line(self, line):
        if "Done reindexing" in line:
            return ProcessorResult(
                self.break_on_finish,
                "Reindexing done!",
            )
        elif "reindexing blockchain" in line:
            print("Reindexing started!")
        else:
            found = self.replaying_regexp.findall(line)
            if found:
                last_group = found.pop()
                assert len(last_group) >= 3
                block_num = int(last_group[1])
                all_blocks = int(last_group[2])
                print(f"Latest block number {block_num} from {all_blocks}")

        return ProcessorResult()

    def finalize(self):
        pass
