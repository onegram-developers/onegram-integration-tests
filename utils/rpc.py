import json
import websocket
import requests
import ssl
import re
import threading

class ConnectionDoesNotExistsException(Exception):
    pass


class ObjectDoesNotExistsException(Exception):
    pass


class RPCErrorException(Exception):
    pass


INVALID_URL = "Invalid node url!"


class RPC:
    """generic RPC for rpc-node & cli_wallet as well. Supports http & websockets"""
    def __init__(self, node_url, port=None):
        if (not "http" in node_url) and (not "ws" in node_url):
            raise ValueError(INVALID_URL)

        self.json_indent = 2
        if port is None:
            self.node_url = node_url
        else:
            self.node_url = "%s:%d/rpc" % (node_url, port)

        self.session_id_counter = 0
        self.session = None

    def __del__(self):
        self.close_session()

    def _keep_alive(self, interval, event):
        while not event.wait(interval):
            if self.session is not None:
                try:
                    self.session.ping()
                except Exception as ex:
                    break

    def close_session(self):
        if self.session is not None:
            self.session.close()
            self.session = None

    def open_session(self):
        self.session_id_counter = 0

        self.close_session()

        if "http" in self.node_url:
            self.session = requests.Session()
        elif "ws" in self.node_url:
            self.session = websocket.create_connection(
                self.node_url,
                timeout=6000,
                sslopt={"cert_reqs": ssl.CERT_NONE}
            )
            event = threading.Event()
            thread = threading.Thread(target=self._keep_alive, args=(60, event))
            thread.setDaemon(True)
            thread.start()
        else:
            raise ValueError(INVALID_URL)

    def session_closed(self):
        if self.session is None:
            return True
        else:
            return False

    def ensure_session_exists(self):
        if self.session_closed():
            self.open_session()


    @staticmethod
    def parse_rpc_response(msg):
        msg_json = json.loads(msg)
        if "error" in msg_json:
            err = msg_json["error"]
            assert("message" in err)
            err_msg = err["message"]
            err_data = err["data"]
            raise RPCErrorException(f"{err_msg} : {json.dumps(err_data, indent=2)} ")
        else:
            assert("result" in msg_json)
            return msg_json["result"]

    def make_query(self, method_name, args):
        query = {
            "jsonrpc": "2.0",
            "id": self.session_id_counter,
            "method": method_name,
            "params": args,
        }
        query_string = json.dumps(query, indent=self.json_indent, ensure_ascii=False).encode('utf8')
        return query_string

    def __getattr__(self, method_name):
        self.session_id_counter += 1
        self.ensure_session_exists()
        if "http" in self.node_url:
            def http_rpc(*args):
                try:
                    query_string = self.make_query(method_name, args)
                    response = self.session.post(self.node_url, query_string)
                # TODO: better exception handling
                except requests.exceptions.ConnectionError:
                    raise ConnectionDoesNotExistsException()

                return self.parse_rpc_response(response.text)

            return http_rpc
        elif "ws" in self.node_url:
            def websocket_rpc(*args):
                try:
                    query_string = self.make_query(method_name, args)
                    self.session.send(query_string)
                    response = self.session.recv()
                # TODO: better exception handling
                except websocket.WebSocketConnectionClosedException:
                    raise ConnectionDoesNotExistsException()

                return self.parse_rpc_response(response)

            return websocket_rpc
        else:
            raise ValueError(INVALID_URL)


def is_id_string(identifier):
    return re.match(r"^[0-9]\.[1-9]?[0-9]+\.[0-9]+$", identifier)


class BaseRPC:
    def __init__(self, rpc):
        self.rpc = rpc

    @classmethod
    def from_url(cls, node_url):
        return cls(RPC(node_url))

    @classmethod
    def from_network_info(cls, network_info):
        rpc_url = f'ws://{network_info.hostname}:{network_info.host_port}'
        return cls.from_url(rpc_url)

    def __getattr__(self, method_name):
        return self.rpc.__getattr__(method_name)

    def connect(self):
        self.rpc.open_session()

    def disconnect(self):
        self.rpc.close_session()

    def is_alive(self):
        props = None
        try:
            props = self.get_global_properties()
        finally:
            return props is not None


class ApiAccess:

    def __init__(self, rpc, api_id):
        self.rpc = rpc
        self.id = api_id

    def __getattr__(self, method_name):
        def call_method(*args):
            return self.rpc.call(self.id, method_name, args)

        return call_method

    def __call__(self, *args, **kwargs):
        return self.__getattr__(kwargs["method"])(args)


class WitnessNodeRPC(BaseRPC):

    """
    Usage:
        rpc = utils.rpc.WitnessNodeRPC.from_url(url)
        rpc.block_api.get_block(1) # rpc.api_name.method()
        rpc.get_global_properties() # query to database api
    """

    available_apis = [
        "database_api",
        "login_api",
        "block_api",
        "network_broadcast_api",
        "history_api",
        "archive_api",
        "network_node_api",
        "crypto_api",
        "asset_api",
        "orders_api",
        "debug_api"
    ]

    def __init__(self, rpc):
        self.rpc = rpc
        # database api is always 0

        self.apis = {}
        self.apis["database_api"] = ApiAccess(self.rpc, 0)
        # database api is always 1
        self.apis["login_api"] = ApiAccess(self.rpc, 1)

    def __getattr__(self, api_name):
        if api_name in self.available_apis:
            # add api to list
            if api_name not in self.apis:
                assert api_name.endswith("_api")
                name = api_name[:(len(api_name) - 4)]
                api_id = self.login_api(method=name)
                self.apis[api_name] = ApiAccess(self.rpc, api_id)

            return self.apis[api_name]

        elif api_name == "db":
            return self.rpc
        else:
            return self.rpc.__getattr__(api_name)  # use base rpc

    def login(self, user_name="", password=""):
        # login api calls login method
        self.login_api.login(user_name, password)

    def get_head_block_num(self):
        props = self.get_dynamic_global_properties()

        if props is None:
            raise ObjectDoesNotExistsException()

        return props["head_block_number"]

    def get_head_block(self):
        block_number = self.get_head_block_num()
        block = self.get_block(block_number)

        if block is None:
            raise ObjectDoesNotExistsException()

        return block

    def get_object(self, object_id):
        assert is_id_string(object_id)
        objects = self.get_objects([object_id])

        if (objects is None) or (len(objects) <= 0) or (objects[0] is None):
            raise ObjectDoesNotExistsException()

        return objects[0]

    def get_account(self, account_name_or_id):
        if is_id_string(account_name_or_id):
            return self.get_object(account_name_or_id)
        else:
            accounts = self.lookup_account_names([account_name_or_id])

            if (accounts is None) or (len(accounts) <= 0) or (accounts[0] is None):
                raise ObjectDoesNotExistsException()

            return accounts[0]

    def get_asset(self, asset_symbol_or_id):
        if is_id_string(asset_symbol_or_id):
            return self.get_object(asset_symbol_or_id)
        else:
            assets = self.lookup_asset_symbols([asset_symbol_or_id])

            if (assets is None) or (len(assets) <= 0):
                raise ObjectDoesNotExistsException()

            return assets[0]

    def get_config(self):
        return self.get_object("2.0.0")

    def get_chain_parameters(self):
        cfg = self.get_config()
        return cfg["parameters"]

    def get_balance(self, account_name_or_id, asset_symbol_or_id):
        account_id = account_name_or_id
        if not is_id_string(account_id):
            account_id = self.get_account(account_name_or_id)["id"]

        asset_id = asset_symbol_or_id
        if not is_id_string(asset_id):
            asset_id = self.get_asset(asset_symbol_or_id)["id"]

        balances = self.get_account_balances(account_id, [asset_id])
        if (balances is None) or (len(balances) <= 0) or (balances[0] is None):
            raise ObjectDoesNotExistsException()

        return balances[0]

    def get_vesting_balances_ex(self, account_name_or_id, asset_symbol_or_id=None):
        account_id = account_name_or_id
        if not is_id_string(account_id):
            account_id = self.get_account(account_name_or_id)["id"]

        v_balances = self.get_vesting_balances(account_id)

        if asset_symbol_or_id is None:
            return v_balances
        else:
            asset_id = asset_symbol_or_id
            if not is_id_string(asset_id):
                asset_id = self.get_asset(asset_symbol_or_id)["id"]

            result = []
            for v_balance in v_balances:
                balance = v_balance["balance"]
                if balance["asset_id"] == asset_id:
                    result.append(v_balance)

            return result

    def get_vesting_balances_sum(self, account_name_or_id, asset_symbol_or_id=None):
        v_balances = self.get_vesting_balances_ex(account_name_or_id, asset_symbol_or_id)

        amount_sum = 0

        for v_balance in v_balances:
            balance = v_balance["balance"]
            amount_sum += balance["amount"]

        return amount_sum

    def get_balance_amount(self, account_name_or_id, asset_symbol_or_id):
        balance = self.get_balance(account_name_or_id, asset_symbol_or_id)
        return int(balance["amount"])

    def get_operation_fee(self, op_id):
        global_props = self.get_global_properties()
        fees = global_props["parameters"]["current_fees"]

        assert op_id < len(fees["parameters"])

        fee = fees["parameters"][op_id]
        assert fee[0] == op_id

        return fee[1]


# TODO add some important cli-wallet specific methods
class CliWalletRPC(BaseRPC):
    pass
