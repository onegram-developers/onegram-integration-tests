
def sum_transaction_fee(transaction):
    operations = transaction["operations"]
    assert len(operations) > 0

    fee_sum = 0
    for op_id, op in operations:
        fee_sum += int(op["fee"]["amount"])

    return fee_sum


def sum_transfer_amount(transaction, asset_id=None):
    operations = transaction["operations"]
    assert len(operations) > 0

    amount_sum = 0
    for op_id, op in operations:
        if op_id == 0:  # tranfer only
            if (asset_id is None) or(op["amount"]["asset_id"] == asset_id):
                amount_sum += int(op["amount"]["amount"])

    return amount_sum


def div_up(a, b):
    return int((a + b - 1) / b)


def ceil(num, significance):
    return div_up(num, significance) * significance


# see: https://gitlab.com/cryptohouse/OneGramDev/wikis/memo
def calc_serialized_memo_size(memo):
    msg_size = len(memo)
    compressed_size = div_up(int.bit_length(msg_size), 7)
    header_size = 75 + compressed_size
    encrypted_msg_size = ceil(msg_size + 4, 16)  # + 4 for checksum
    return header_size + encrypted_msg_size


def calc_memo_fee(memo, price_per_kb):
    size = calc_serialized_memo_size(memo)
    return int((size * price_per_kb) / 1024)


# this function doesn't take in account minimal & maximal fee
def calc_transfer_fee(amount_in_satoshis, price):
    return int(amount_in_satoshis * price / 10000)


def generate_memo(size):
    allowed_chars = "TEST MEMO!"
    memo = ""
    for i in range(0, size):
        memo += allowed_chars[i % len(allowed_chars)]
    return memo


def format_asset_amount(amount, precision):
    amount_str = "{1:,.{0}f}".format(precision, amount)
    return amount_str
