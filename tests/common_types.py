import typing

from utils.rpc import WitnessNodeRPC, CliWalletRPC

Account = typing.NamedTuple('Account', [
    ('name', typing.Text),
    ('wif_key', typing.Text),
])

# Testnet should be:
#  - trusted rpc node
#  - cli_wallet rpc
#  - other nodes (endpoints) of testnet
PrivateTestnet = typing.NamedTuple('PrivateTestnet', [
        ('trusted_node', WitnessNodeRPC),
        ('cli_wallet', CliWalletRPC),
        ('accounts', typing.List[Account])
    ])




