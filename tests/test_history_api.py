import json
import re

# https://dev.bitshares.works/en/master/api/blockchain_api/history.html#account-history

# get_account_history
# get_account_history_by_operations
# get_account_history_operations
# get_relative_account_history


account_id_pattern = re.compile(r"\"(1\.2\.[0-9]+)\"")


def get_last_op(node):
    props = node.get_dynamic_global_properties()
    op_id = props['last_operation_id']
    return node.get_object(op_id)


def find_account_id_in_op(op):
    g = account_id_pattern.findall(json.dumps(op))
    return g[0]


def find_all_account_ids_in_op(op):
    return account_id_pattern.findall(json.dumps(op))


def test_get_account_history(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node
    api = node.history_api

    expected_keys = {
        "op",
        "result",
        "block_num",
        "trx_in_block",
        "op_in_trx",
        "virtual_op",
    }

    last_op = get_last_op(node)
    account_id = find_account_id_in_op(last_op)

    result = api.get_account_history(account_id, '1.11.0', 100, last_op['id'])

    # test query by name is same as query by id
    acc_object = node.get_account(account_id)
    assert api.get_account_history(acc_object['name'], '1.11.0', 100, last_op['id']) == result

    # there should be the last operation for sure
    assert len(result) >= 1
    assert last_op in result

    # test if every operation contains specified account
    for op in result:
        acc_ids = find_all_account_ids_in_op(op)
        assert account_id in acc_ids

        for key in expected_keys:
            assert key in op


def test_get_account_history_by_operations(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node
    api = node.history_api

    last_op = get_last_op(node)
    account_id = find_account_id_in_op(last_op)

    limit = 100
    start = int(last_op['id'].split('.')[2]) - limit

    hist_detail = api.get_account_history_by_operations(account_id, [], start, limit)
    history = api.get_relative_account_history(account_id, start, limit, start + limit - 1)

    assert hist_detail['operation_history_objs'] == history

    last_op_id = last_op['op'][0]  # op id
    result = api.get_account_history_by_operations(account_id, [last_op_id], start, 100)

    assert 'total_count' in result
    for op in result['operation_history_objs']:
        assert op in history
        assert op['op'][0] == last_op_id
        acc_ids = find_all_account_ids_in_op(op)
        assert account_id in acc_ids


def test_get_account_history_operations(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node
    api = node.history_api

    last_op = get_last_op(node)
    account_id = find_account_id_in_op(last_op)

    limit = 100

    history = api.get_account_history(account_id, '1.11.0', limit, last_op['id'])

    # result of this operation should be subset of 'history'
    op_type = last_op['op'][0]
    history_ops = api.get_account_history_operations(account_id, op_type, '1.11.0', last_op['id'], limit)
    for op in history_ops:
        assert op in history
        assert op['op'][0] == op_type
        acc_ids = find_all_account_ids_in_op(op)
        assert account_id in acc_ids


def test_get_relative_account_history(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node
    api = node.history_api

    limit = 100
    last_op = get_last_op(node)
    account_id = find_account_id_in_op(last_op)
    history = api.get_relative_account_history(account_id, 0, limit, 0)

    assert len(history) > 0
    assert len(history) <= limit
    for op in history:
        acc_ids = find_all_account_ids_in_op(op)
        assert account_id in acc_ids


def test_get_last_operations_history(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node
    api = node.history_api
    limit = 100

    history = api.get_last_operations_history(limit)
    op_ids = list(map(lambda op: op['id'], history))

    last_op = get_last_op(node)
    last_op_id = last_op['id']
    last_op_instance = int(last_op_id.split('.')[2])

    assert len(op_ids) == limit or last_op_instance <= limit

    for i in reversed(range(0, limit)):
        op_id = f"1.11.{last_op_instance - i}"
        assert op_id in op_ids




