import utils


def test_core_asset_symbol(testnet_with_ico_account):
    node = testnet_with_ico_account.trusted_node
    ico_account = testnet_with_ico_account.accounts[0]

    asset_id = "1.3.0"

    asset = node.get_asset(asset_id)
    assert asset["symbol"] == "OGC"

    balance = node.get_balance(ico_account.name, asset_id)
    assert balance

    assert int(balance["amount"]) > 0
    assert balance["asset_id"] == asset["id"]


def test_core_asset_precision(testnet_with_accounts):
    node = testnet_with_accounts.trusted_node
    wallet = testnet_with_accounts.cli_wallet
    ico_account = testnet_with_accounts.accounts[0]
    account = testnet_with_accounts.accounts[1]

    asset_id = "1.3.0"
    asset_object = node.get_asset(asset_id)
    assert asset_object

    asset_symbol = asset_object["symbol"]
    assert asset_symbol == "OGC"

    precision = asset_object["precision"]
    decimals = 10 ** precision

    amount = 1 / decimals  # one satoshi
    amount_str = utils.format_asset_amount(amount, precision)

    before_balance = node.get_balance(account.name, asset_id)

    transaction = wallet.transfer(ico_account.name, account.name, amount_str, asset_symbol, "", True)
    assert transaction

    transaction_amount = utils.sum_transfer_amount(transaction, asset_id)

    assert transaction_amount == 1

    after_balance = node.get_balance(account.name, asset_id)

    balance_diff = int(after_balance["amount"]) - int(before_balance["amount"])

    assert balance_diff == 1, "Transferred amount should be 1 satoshi"
