import random
from functools import reduce

# Accounts
# get_key_references
# get_accounts
# get_full_accounts
# get_account_by_name
# get_account_references
# lookup_account_names
# lookup_accounts
# get_account_count

# https://dev.bitshares.works/en/master/api/blockchain_api/database.html#accounts


def check_is_unique(container):
    return len(container) == len(set(container))


def get_random_account_ids(node, count):
    max_id = node.get_account_count() - 1
    ids = [f"1.2.{random.randint(0, max_id)}" for j in range(count)]
    return ids


def test_get_key_references(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    assert node.get_key_references([]) == []

    # generic test
    for count in range(1, 4):
        ids = get_random_account_ids(node, count)
        keys = list(map(lambda acc_id: node.get_account(acc_id)["owner"]["key_auths"][0][0], ids))
        accounts = node.get_key_references(keys)
        assert len(accounts) == count
        for acc in accounts:
            assert check_is_unique(acc)


def test_get_accounts(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    expected_keys = {
        'id',
        'membership_expiration_date',
        'registrar',
        'referrer',
        'lifetime_referrer',
        'network_fee_percentage',
        'lifetime_referrer_fee_percentage',
        'referrer_rewards_percentage',
        'name',
        'owner',
        'active',
        'options',
        'statistics',
        'whitelisting_accounts',
        'blacklisting_accounts',
        'whitelisted_accounts',
        'blacklisted_accounts',
        'owner_special_authority',
        'active_special_authority',
        'top_n_control_flags',
    }

    # test empty query
    assert node.get_accounts([]) == []

    # test query by name & query by id
    assert node.get_accounts(["ico1"]) == node.get_accounts(["1.2.6"])

    # test keys
    it_count = 10
    for i in range(0, it_count):
        instance = random.randint(0, 1314)
        acc_id = f"1.2.{instance}"
        accounts = node.get_accounts([acc_id])
        assert len(accounts) == 1
        for expected_key in expected_keys:
            assert expected_key in accounts[0]

    # multiple account query
    for i in range(1, it_count+1):
        ids = get_random_account_ids(node, i)
        accounts = node.get_accounts(ids)
        assert len(ids) == len(accounts)


def test_get_full_accounts(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    expected_keys = {
        'statistics',
        'registrar_name',
        'referrer_name',
        'lifetime_referrer_name',
        'votes',
        # 'cashback_balance',  TODO: in the future
        'balances',
        'vesting_balances',
        'limit_orders',
        'call_orders',
        'settle_orders',
        'proposals',
        'assets',
        # 'withdraws_from',  TODO: in the future
        # 'withdraws_to',  TODO: in the future
        # 'htlcs_from',  TODO: in the future
        # 'htlcs_to',  TODO: in the future
        # 'more_data_available',  TODO: in the future
    }

    ids = get_random_account_ids(node, 100)
    for acc_id in ids:
        accounts = node.get_accounts([acc_id])
        full_accounts = node.get_full_accounts([acc_id], False)
        account_pair = full_accounts[0]
        assert len(accounts) == len(full_accounts)
        assert account_pair[0] == acc_id
        assert accounts[0] == account_pair[1]['account']

        for expected_key in expected_keys:
            assert expected_key in account_pair[1]


def test_get_account_by_name(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    ids = get_random_account_ids(node, 100)
    for acc_id in ids:
        accounts = node.get_accounts([acc_id])
        account = node.get_account_by_name(accounts[0]['name'])

        assert account == accounts[0]


# TODO: make tests more generic e.g. for different blockchains than onegram testnet
def test_get_account_references(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    expected_values = {
        9: ['1.2.1'],
        10: ['1.2.1'],
        11: ['1.2.1'],
        12: ['1.2.1'],
        13: ['1.2.1'],
        14: ['1.2.1'],
        15: ['1.2.1'],
        16: ['1.2.1'],
        17: ['1.2.1'],
        18: ['1.2.1'],
        30: ['1.2.0', '1.2.2'],
        31: ['1.2.0', '1.2.2'],
        32: ['1.2.0', '1.2.2'],
    }

    for key, value in expected_values.items():
        acc_id = f"1.2.{key}"
        accounts = node.get_accounts([acc_id])
        refs_by_id = node.get_account_references(acc_id)
        refs_by_name = node.get_account_references(accounts[0]['name'])
        assert refs_by_id == value
        assert refs_by_id == refs_by_name


def maybe_get_name(node, acc_id, fake_name):
    return fake_name if random.randint(0, 1) == 0 else node.get_accounts([acc_id])[0]['name']


def test_lookup_account_names(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    account = node.get_account("1.2.0")

    assert node.lookup_account_names([]) == []
    assert node.lookup_account_names([account['name']]) == node.get_accounts([account['name']])
    assert node.lookup_account_names(["I would be really surprised if account with this name would exist"]) == [None]

    fake_name = "blah-blah-blah"
    it_count = 15
    for i in range(1, it_count):
        ids = get_random_account_ids(node, i)
        account_names = list(map(lambda acc_id: maybe_get_name(node, acc_id, fake_name), ids))
        accounts = node.lookup_account_names(account_names)
        assert len(accounts) == i
        assert len(accounts) == len(account_names)
        bools = map(lambda x, y: (x is None) == (y == fake_name), accounts, account_names)
        assert reduce(lambda x, y: x and y, bools)


def test_lookup_accounts(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node
    result = node.lookup_accounts("ico", 100, False)
    assert len(result) <= 100


def test_get_account_count(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node
    assert node.get_account_count() >= 0


