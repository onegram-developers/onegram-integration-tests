import random
import pytest
# see: https://dev.bitshares.works/en/master/api/blockchain_api/database.html#blocks-and-transactions

# Blocks and transactions
# get_block_header
# get_block
# get_transaction
# get_recent_transaction_by_id


def compare_headers(a, b):
    for h_key, h_value in a.items():
        assert h_key in b
        assert h_value == b[h_key]


def test_get_block_header(local_testnet_no_wallet):
    expected_headers = {
        1: {
          'previous': '0000000000000000000000000000000000000000',
          'timestamp': '2018-04-09T14:00:10',
          'witness': '1.6.2',
          'transaction_merkle_root': '0000000000000000000000000000000000000000',
          'extensions': []
        },
        94321: {
          'previous': '00017070283d89880c2fd3259a8f49a656b241a1',
          'timestamp': '2018-04-20T19:01:10',
          'witness': '1.6.3',
          'transaction_merkle_root': '0000000000000000000000000000000000000000',
          'extensions': []
        },
        150000: {
            'previous': '000249ef67cc8d2b5ac75ff6e1442d4945d5fa1a',
            'timestamp': '2018-04-27T08:36:20',
            'witness': '1.6.2',
            'transaction_merkle_root': 'f9960a4aa8690de8543f415e2f979a843d23217e',
            'extensions': []
        },
    }

    node = local_testnet_no_wallet.trusted_node
    for key, value in expected_headers.items():
        header = node.get_block_header(int(key))
        compare_headers(value, header)


def test_get_block(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    expected_signatures = {
        2: '2059c8096f6e71f024313091cb41256afb370539df13533f526425d2d52af4aba0581f7753b348f14760635432a55e32d23212fae0e91c603f638179f90b943cf0',
        100: '200bb7a0f6a41ccd6d5878dd04d5d2931a29364c7b9d263f68f605821012197fbc161251946d3ffdab8c5bbb7b04727426ec46fd2359d1b9235654b65beec315d6',
        148222: '20791e70ed54c2bf8e1b25d868e8e318cb69f9f2b65f267f02755e059da5dcbe661c73e94a131fe9ce0508093efd051efbfcc2ed86a3a46dd00a698eb77618b2c4',
        150222: '20555307b9e2aa9bc6c9037c1e50d330b51957a0c91d3a88843045c5baac20533d153ceb6c4f4c2d9b5786db8b489bd961e436cf15c75f3ce02c22e5aaa8b47231',
    }

    for block_id, signature in expected_signatures.items():
        block = node.get_block(block_id)
        assert block['witness_signature'] == signature

    it_count = 100
    for i in range(0, it_count):
        block_id = random.randint(1, 4000000)
        block = node.get_block(block_id)
        block_header = node.get_block_header(block_id)
        compare_headers(block_header, block)

        assert 'witness_signature' in block
        assert 'transactions' in block
        assert len(block['witness_signature']) == \
               len('2059c8096f6e71f024313091cb41256afb370539df13533f526425d2d52af4aba0581f7753b348f14760635432a55e32d23212fae0e91c603f638179f90b943cf0')


def test_get_transaction(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node
    from_block = 150000
    to_block = 160000
    it_count = 5
    for i in range(0, it_count):
        block_num = random.randint(from_block, to_block)
        block_trxs = node.get_block(block_num)["transactions"]
        for trx_num in range(0, len(block_trxs)):
            block_trx = block_trxs[trx_num]
            trx = node.get_transaction(block_num, trx_num)
            assert block_trx == trx


@pytest.mark.skip(reason="we need trx id, and I don't know where to find one (it's trx hash)")
def test_get_recent_transaction_by_id(local_testnet_no_wallet):
    pass




