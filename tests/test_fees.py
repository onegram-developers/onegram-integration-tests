from tests import PrivateTestnet
import utils
import random
from datetime import datetime
import time
import pytest
import sys

# TODO: parametrize asset symbol ("OGC")


def test_feeless_account(testnet_with_ico_account: PrivateTestnet):
    node = testnet_with_ico_account.trusted_node
    wallet = testnet_with_ico_account.cli_wallet
    ico_account = testnet_with_ico_account.accounts[0]

    from_acc = ico_account.name
    to_acc = "user1"
    amount = 100
    asset = "OGC"

    balance_amount_before = node.get_balance_amount(from_acc, asset)

    transaction = wallet.transfer(from_acc, to_acc, amount, asset, "", True)
    assert utils.sum_transaction_fee(transaction) == 0, "Fee should be zero"

    memo = "Hello my dear friend, I am sending you some cash, so please use it wisely! Your friendly ICO account."

    transaction = wallet.transfer(from_acc, to_acc, amount, asset, memo, True)
    assert utils.sum_transaction_fee(transaction) == 0, "Fee should be zero"

    balance_amount_after = node.get_balance_amount(from_acc, asset)

    asset_object = node.get_asset(asset)
    assert asset_object

    total_amount = (2 * amount) * 10**asset_object["precision"]

    assert ((balance_amount_before - total_amount) == balance_amount_after), "Fee should be zero"


@pytest.mark.skip(reason="need to update genesis files for mainnet/testnet")
def test_minimal_fee(testnet_with_accounts: PrivateTestnet):
    node = testnet_with_accounts.trusted_node
    wallet = testnet_with_accounts.cli_wallet
    accounts = testnet_with_accounts.accounts

    from_account = accounts[1]
    to_account = accounts[2]

    asset = "OGC"
    asset_object = node.get_asset(asset)
    assert asset_object

    precision = asset_object["precision"]
    decimals = 10 ** precision

    minimal_fee = 1

    transfer_fee = node.get_operation_fee(0)
    percentage_fee = transfer_fee["percentage"]
    price_per_kb = transfer_fee["price_per_kbyte"]

    memo = "Some longer memo"
    memo_fee = utils.calc_memo_fee(memo, price_per_kb)

    max_value_with_minimal_fee = int(10000 / percentage_fee)
    for i in range(1, max_value_with_minimal_fee):
        amount = i / decimals
        amount_str = utils.format_asset_amount(amount, precision)

        transaction = wallet.transfer(from_account.name, to_account.name, amount_str, asset, "", True)
        assert utils.sum_transaction_fee(transaction) == minimal_fee, \
            "Minimal fee should be equal to one 'satoshi'"

        transaction = wallet.transfer(from_account.name, to_account.name, amount_str, asset, memo, True)
        assert utils.sum_transaction_fee(transaction) == (minimal_fee + memo_fee), \
            "Minimal fee should be equal to one 'satoshi'"


def test_maximal_fee(testnet_with_accounts: PrivateTestnet):
    node = testnet_with_accounts.trusted_node
    wallet = testnet_with_accounts.cli_wallet
    accounts = testnet_with_accounts.accounts

    from_account = accounts[1]
    to_account = accounts[2]

    asset = "OGC"

    transfer_fee = node.get_operation_fee(0)
    maximal_fee = transfer_fee["percentage_max_fee"]
    percentage_fee = transfer_fee["percentage"]
    price_per_kb = transfer_fee["price_per_kbyte"]

    asset_object = node.get_asset(asset)
    assert asset_object

    precision = asset_object["precision"]
    decimals = 10 ** precision

    amount = ((2 * maximal_fee) / decimals) * (10000 / percentage_fee)
    amount_str = utils.format_asset_amount(amount, precision)

    transaction = wallet.transfer(from_account.name, to_account.name, amount_str, asset, "", True)
    assert utils.sum_transaction_fee(transaction) == maximal_fee, \
        "Fee should be equal to maximal fee"

    chain_params = node.get_chain_parameters()
    max_trx_size = chain_params["maximum_transaction_size"]
    trx_size_without_memo = sys.getsizeof(transaction)

    assert max_trx_size > trx_size_without_memo

    memo = utils.generate_memo((max_trx_size - trx_size_without_memo) // 2)
    memo_fee = utils.calc_memo_fee(memo, price_per_kb)

    transaction = wallet.transfer(from_account.name, to_account.name, amount_str, asset, memo, True)
    assert utils.sum_transaction_fee(transaction) == (maximal_fee + memo_fee), \
        "Fee should be equal to maximal fee"


@pytest.mark.flaky(reruns=10)
def test_transfer_fee(testnet_with_accounts: PrivateTestnet):
    node = testnet_with_accounts.trusted_node
    wallet = testnet_with_accounts.cli_wallet
    accounts = testnet_with_accounts.accounts

    asset = "OGC"
    asset_object = node.get_asset(asset)
    assert asset_object

    precision = asset_object["precision"]
    decimals = 10 ** precision

    transfer_fee = node.get_operation_fee(0)
    percentage_fee = transfer_fee["percentage"]
    maximal_fee = transfer_fee["percentage_max_fee"]
    price_per_kb = transfer_fee["price_per_kbyte"]

    memo = "Some longer memo"
    memo_fee = utils.calc_memo_fee(memo, price_per_kb)

    max_value_with_minimal_fee = int(10000 / percentage_fee)
    min_value_with_maximal_fee = int(maximal_fee * (10000 / percentage_fee))

    normal_account_count = len(accounts) - 1  # minus ico_account

    random.seed()
    it_count = 500  # after 500 iterations we usually don't have enough money to send :-/
    for i in range(1, it_count):
        # TODO: do this more transparently by adding 'fee_less' flag to Account class
        from_account = accounts[i % normal_account_count + 1]   # we skip ico_account as it is fee less
        to_account = accounts[(i+1) % normal_account_count + 1]

        rand_value = random.randint(max_value_with_minimal_fee, min_value_with_maximal_fee)
        amount = rand_value / decimals  # minimal transfer
        amount_str = utils.format_asset_amount(amount, precision)

        fee = utils.calc_transfer_fee(rand_value, percentage_fee)

        transaction = wallet.transfer(from_account.name, to_account.name, amount_str, asset, "", True)
        assert utils.sum_transaction_fee(transaction) == fee, f"Transfer fee should be equal to {fee}"

        transaction = wallet.transfer(from_account.name, to_account.name, amount_str, asset, memo, True)
        assert utils.sum_transaction_fee(transaction) == (fee + memo_fee), \
            f"Transfer with memo fee should be equal to {fee + memo_fee}"


# TODO: we should change maintenance interval for this test (right now it is 30min)
# We have to wait for next maintenance interval to apply changes:
# http://docs.bitshares.org/user/account-memberships.html
#
# 'fees' account A.K.A. sticky lifetime referrer
@pytest.mark.skip(reason="need to change maintenance interval for this test")
def test_lifetime_referrer_fee(testnet_with_accounts: PrivateTestnet):
    node = testnet_with_accounts.trusted_node
    wallet = testnet_with_accounts.cli_wallet
    accounts = testnet_with_accounts.accounts

    from_account = accounts[1]
    to_account = accounts[2]

    chain_props = node.get_chain_properties()

    sticky_lifetime_referrer_id = chain_props["enforced_lifetime_referrer"]
    assert sticky_lifetime_referrer_id

    asset = "OGC"
    asset_object = node.get_asset(asset)
    assert asset_object

    precision = asset_object["precision"]
    decimals = 10 ** precision

    memo = "Some longer memo"

    balance_amount_before = node.get_vesting_balances_sum(sticky_lifetime_referrer_id, asset)

    props = node.get_global_properties()
    params = props["parameters"]
    lifetime_referrer_percent_of_fee = int(params["lifetime_referrer_percent_of_fee"])

    fee_sum = 0

    from_account_obj = node.get_account(from_account.name)
    referrer_id = from_account_obj["lifetime_referrer"]
    referrer_account_obj = node.get_account(referrer_id)
    assert sticky_lifetime_referrer_id == referrer_account_obj["id"]

    # Local vs Global fee values
    assert int(from_account_obj["lifetime_referrer_fee_percentage"]) == lifetime_referrer_percent_of_fee, \
        "Local lifetime referrer fee should be equal to global"

    #random.seed()
    it_count = 10
    for i in range(1, it_count):

        rand_value = random.randint(1, 200)
        amount = rand_value / decimals  # minimal transfer
        amount_str = utils.format_asset_amount(amount, precision)

        transaction = wallet.transfer(from_account.name, to_account.name, amount_str, asset, "", True)
        fee_sum += utils.sum_transaction_fee(transaction)

        transaction = wallet.transfer(from_account.name, to_account.name, amount_str, asset, memo, True)
        fee_sum += utils.sum_transaction_fee(transaction)

    dyn_props = node.get_dynamic_global_properties()
    next_maintenance = dyn_props["next_maintenance_time"]
    time_now = datetime.utcnow()
    time_to_wait = datetime.strptime(next_maintenance, "%Y-%m-%dT%H:%M:%S") - time_now

    print(f"Time now: {time_now}")
    print(f"Next maintenance: {next_maintenance}")

    max_wait_time_sec = 300  # 5 minutes
    time_to_wait_sec = time_to_wait.total_seconds()
    assert time_to_wait_sec <= max_wait_time_sec, \
        f"Need to wait more than {max_wait_time_sec} seconds: {time_to_wait_sec}. Next maintenance: {next_maintenance}"

    time_to_wait_sec += 5  # just in case we wait little bit more
    print(f"Waiting for next maintenance interval {time_to_wait_sec} seconds")
    time.sleep(time_to_wait_sec)

    balance_amount_after = node.get_vesting_balances_sum(sticky_lifetime_referrer_id, asset)
    balance_diff = int(balance_amount_after - balance_amount_before)

    assert int(fee_sum * lifetime_referrer_percent_of_fee / 10000) == balance_diff
