import time
from datetime import datetime

# see: https://dev.bitshares.works/en/master/api/blockchain_api/database.html#globals

# Globals
# get_chain_properties
# get_global_properties
# get_config
# get_chain_id
# get_dynamic_global_properties


def time2str(t):
    return f"{t:%Y-%m-%dT%H:%M:%S}"


def str2time(s):
    return datetime.strptime(s, "%Y-%m-%dT%H:%M:%S")


def check_values_list(expected, actual):
    assert len(expected) == len(actual)
    for i in range(0, len(expected)):
        if isinstance(expected[i], dict):
            check_values_dict(expected[i], actual[i])
        elif isinstance(expected[i], list):
            check_values_list(expected[i], actual[i])
        else:
            assert expected[i] == actual[i]


def check_values_dict(expected, actual):
    for key, value in expected.items():
        assert key in actual
        if isinstance(value, dict):
            check_values_dict(value, actual[key])
        elif isinstance(value, list):
            check_values_list(value, actual[key])
        else:
            assert value == actual[key]


def test_get_chain_properties(local_testnet_no_wallet):
    expected = {
      'id': '2.11.0',
      'chain_id': '10ba5bd926fc0541dcfea83593fad914cde23978f01445192b174db51899f633',
      'immutable_parameters': {
        'min_committee_member_count': 3,
        'min_witness_count': 11,
        'num_special_accounts': 0,
        'num_special_assets': 0,
        'feeless_accounts': {
          'account_names': [
            'com1',
            'com2',
            'com3',
            'fees',
            'ico1',
            'ico2'
          ]
        },
        'eternal_committee_members': {
          'account_names': [
            'com1',
            'com2',
            'com3'
          ]
        },
        'sticky_lifetime_referrers': {
          'referrer_names': [
            'fees'
          ]
        },
        'enforced_lifetime_referrer': 'fees'
      },
      'feeless_accounts': {
        'account_ids': [
          '1.2.6',
          '1.2.7',
          '1.2.8',
          '1.2.30',
          '1.2.31',
          '1.2.32'
        ]
      },
      'eternal_committee_accounts': {
        'vote_ids': [
          '0:11',
          '0:12',
          '0:13'
        ]
      },
      'sticky_lifetime_referers': {
        'referrer_ids': [
          '1.2.8'
        ]
      },
      'enforced_lifetime_referrer': '1.2.8'
    }

    actual = local_testnet_no_wallet.trusted_node.get_chain_properties()

    check_values_dict(expected, actual)


def test_get_global_properties(local_testnet_no_wallet):
    keys = [
        'id',
        'parameters',
        'next_available_vote_id',
        'active_committee_members',
        'active_witnesses',
    ]

    expected_params = {
        'current_fees': None,
        'current_operations_permissions': None,
        'block_interval': 10,
        'maintenance_interval': 1800,
        'maintenance_skip_slots': 3,
        'committee_proposal_review_period': 14400,
        'maximum_transaction_size': 2048,
        'maximum_block_size': 1024000000,
        'maximum_time_until_expiration': 86400,
        'maximum_proposal_lifetime': 2419200,
        'maximum_asset_whitelist_authorities': 10,
        'maximum_asset_feed_publishers': 10,
        'maximum_witness_count': 1001,
        'maximum_committee_count': 21,
        'maximum_authority_membership': 10,
        'reserve_percent_of_fee': 0,
        'network_percent_of_fee': 250,
        'lifetime_referrer_percent_of_fee': 9750,
        'cashback_vesting_period_seconds': 3600,
        'cashback_vesting_threshold': '12400786000000',
        'count_non_member_votes': True,
        'allow_non_member_whitelists': False,
        'witness_pay_per_block': 1000000,
        'witness_pay_vesting_seconds': 21600,
        'worker_budget_per_day': 0,
        'max_predicate_opcode': 1,
        'fee_liquidation_threshold': 100000000,
        'accounts_per_fee_scale': 10000,
        'account_fee_scale_bitshifts': 0,
        'max_authority_depth': 2,
        'extensions': None,
    }

    props = local_testnet_no_wallet.trusted_node.get_global_properties()

    for key in keys:
        assert key in props

    assert props['id'] == '2.0.0'
    params = props['parameters']
    for expected_key, expected_value in expected_params.items():
        assert expected_key in params
        assert expected_value is None or expected_value == params[expected_key]


def test_get_config(local_testnet_no_wallet):
    expected_keys = [
        "GRAPHENE_SYMBOL",
        "GRAPHENE_ADDRESS_PREFIX",
        "GRAPHENE_MIN_ACCOUNT_NAME_LENGTH",
        "GRAPHENE_MAX_ACCOUNT_NAME_LENGTH",
        "GRAPHENE_MIN_ASSET_SYMBOL_LENGTH",
        "GRAPHENE_MAX_ASSET_SYMBOL_LENGTH",
        "GRAPHENE_MAX_SHARE_SUPPLY",
        "GRAPHENE_MAX_SIG_CHECK_DEPTH",
        "GRAPHENE_MIN_TRANSACTION_SIZE_LIMIT",
        "GRAPHENE_MIN_BLOCK_INTERVAL",
        "GRAPHENE_MAX_BLOCK_INTERVAL",
        "GRAPHENE_DEFAULT_BLOCK_INTERVAL",
        "GRAPHENE_DEFAULT_MAX_TRANSACTION_SIZE",
        "GRAPHENE_DEFAULT_MAX_BLOCK_SIZE",
        "GRAPHENE_DEFAULT_MAX_TIME_UNTIL_EXPIRATION",
        "GRAPHENE_DEFAULT_MAINTENANCE_INTERVAL",
        "GRAPHENE_DEFAULT_MAINTENANCE_SKIP_SLOTS",
        "GRAPHENE_MIN_UNDO_HISTORY",
        "GRAPHENE_MAX_UNDO_HISTORY",
        "GRAPHENE_MIN_BLOCK_SIZE_LIMIT",
        "GRAPHENE_BLOCKCHAIN_PRECISION",
        "GRAPHENE_BLOCKCHAIN_PRECISION_DIGITS",
        "GRAPHENE_100_PERCENT",
        "GRAPHENE_1_PERCENT",
        "GRAPHENE_MAX_MARKET_FEE_PERCENT",
        "GRAPHENE_DEFAULT_FORCE_SETTLEMENT_DELAY",
        "GRAPHENE_DEFAULT_FORCE_SETTLEMENT_OFFSET",
        "GRAPHENE_DEFAULT_FORCE_SETTLEMENT_MAX_VOLUME",
        "GRAPHENE_DEFAULT_PRICE_FEED_LIFETIME",
        "GRAPHENE_DEFAULT_MAX_AUTHORITY_MEMBERSHIP",
        "GRAPHENE_DEFAULT_MAX_ASSET_WHITELIST_AUTHORITIES",
        "GRAPHENE_DEFAULT_MAX_ASSET_FEED_PUBLISHERS",
        "GRAPHENE_COLLATERAL_RATIO_DENOM",
        "GRAPHENE_MIN_COLLATERAL_RATIO",
        "GRAPHENE_MAX_COLLATERAL_RATIO",
        "GRAPHENE_DEFAULT_MAINTENANCE_COLLATERAL_RATIO",
        "GRAPHENE_DEFAULT_MAX_SHORT_SQUEEZE_RATIO",
        "GRAPHENE_DEFAULT_MAX_WITNESSES",
        "GRAPHENE_DEFAULT_MAX_COMMITTEE",
        "GRAPHENE_DEFAULT_MAX_PROPOSAL_LIFETIME_SEC",
        "GRAPHENE_DEFAULT_COMMITTEE_PROPOSAL_REVIEW_PERIOD_SEC",
        "GRAPHENE_DEFAULT_NETWORK_PERCENT_OF_FEE",
        "GRAPHENE_DEFAULT_LIFETIME_REFERRER_PERCENT_OF_FEE",
        "GRAPHENE_DEFAULT_CASHBACK_VESTING_PERIOD_SEC",
        "GRAPHENE_DEFAULT_CASHBACK_VESTING_THRESHOLD",
        "GRAPHENE_DEFAULT_BURN_PERCENT_OF_FEE",
        "GRAPHENE_DEFAULT_MAX_ASSERT_OPCODE",
        "GRAPHENE_DEFAULT_FEE_LIQUIDATION_THRESHOLD",
        "GRAPHENE_DEFAULT_ACCOUNTS_PER_FEE_SCALE",
        "GRAPHENE_DEFAULT_ACCOUNT_FEE_SCALE_BITSHIFTS",
        "GRAPHENE_MAX_WORKER_NAME_LENGTH",
        "GRAPHENE_MAX_URL_LENGTH",
        "GRAPHENE_CORE_ASSET_CYCLE_RATE",
        "GRAPHENE_CORE_ASSET_CYCLE_RATE_BITS",
        "GRAPHENE_DEFAULT_WITNESS_PAY_PER_BLOCK",
        "GRAPHENE_DEFAULT_WITNESS_PAY_VESTING_SECONDS",
        "GRAPHENE_DEFAULT_WORKER_BUDGET_PER_DAY",
        "GRAPHENE_COMMITTEE_ACCOUNT",
        "GRAPHENE_WITNESS_ACCOUNT",
        "GRAPHENE_RELAXED_COMMITTEE_ACCOUNT",
        "GRAPHENE_NULL_ACCOUNT",
        "GRAPHENE_TEMP_ACCOUNT",
    ]

    config = local_testnet_no_wallet.trusted_node.database_api.get_config()
    for key in expected_keys:
        assert key in config


def test_get_chain_id(local_testnet_no_wallet):
    expected_id = "10ba5bd926fc0541dcfea83593fad914cde23978f01445192b174db51899f633"
    chain_id = local_testnet_no_wallet.trusted_node.get_chain_id()
    assert expected_id == chain_id


def test_get_dynamic_global_properties(local_testnet_no_wallet):
    # {
    #   'id': '2.1.0',
    #   'head_block_number': 4484178,
    #   'head_block_id': '00446c5262c575539b68c40c6fab0e7cc77d6e15',
    #   'time': '2019-09-19T14:38:20',
    #   'current_witness': '1.6.6',
    #   'next_maintenance_time': '2019-09-19T15:00:00',
    #   'last_budget_time': '2019-09-19T14:30:00',
    #   'witness_budget': 71,
    #   'witness_pay': 0,
    #   'accounts_registered_this_interval': 0,
    #   'recently_missed_count': 0,
    #   'current_aslot': 4486124,
    #   'recent_slots_filled': '340282366920938463463374607431768211455',
    #   'dynamic_flags': 0,
    #   'last_irreversible_block_num': 4484171,
    #   'last_operation_id': '1.11.60831107'
    # }

    expected_keys = [
        'id',
        'head_block_number',
        'head_block_id',
        'time',
        'current_witness',
        'next_maintenance_time',
        'last_budget_time',
        'witness_budget',
        'witness_pay',
        'accounts_registered_this_interval',
        'recently_missed_count',
        'current_aslot',
        'recent_slots_filled',
        'dynamic_flags',
        'last_irreversible_block_num',
        'last_operation_id',
    ]

    # check all ids are there
    props_0 = local_testnet_no_wallet.trusted_node.get_dynamic_global_properties()
    for key in expected_keys:
        assert key in props_0

    assert props_0["id"] == '2.1.0'

    config = local_testnet_no_wallet.trusted_node.get_chain_parameters()
    block_interval = config["block_interval"]

    # we wait a bit longer in case we miss some blocks
    time.sleep(4 * block_interval)

    props_1 = local_testnet_no_wallet.trusted_node.get_dynamic_global_properties()
    for key in expected_keys:
        assert key in props_1

    assert props_0["head_block_number"] < props_1["head_block_number"]
    assert props_0["head_block_id"] != props_1["head_block_id"]
    assert props_0["current_witness"] != props_1["current_witness"]
    assert props_0["current_aslot"] < props_1["current_aslot"]
    assert props_0["last_irreversible_block_num"] <= props_1["last_irreversible_block_num"]

    last_op_id_0 = props_0["last_operation_id"].split('.')
    last_op_id_1 = props_1["last_operation_id"].split('.')

    assert int(last_op_id_0[0]) == 1
    assert int(last_op_id_1[0]) == 1
    assert int(last_op_id_0[1]) == 11
    assert int(last_op_id_1[1]) == 11
    assert int(last_op_id_0[2]) <= int(last_op_id_1[2])

    assert str2time(props_0['time']) <= str2time(props_1['time'])



