from tests import PrivateTestnet
import pytest
from utils.rpc import ObjectDoesNotExistsException


def get_account_balances(rpc_node, account_id):
    balances = rpc_node.get_account_balances(account_id, [])
    # filter empty balances
    return [b for b in balances if int(b["amount"]) > 0]


def verify_referrals(net, ltm_accounts=False):
    node = net.trusted_node
    accounts = net.accounts

    chain_props = node.get_chain_properties()
    enforced_lifetime_referrer_id = chain_props["enforced_lifetime_referrer"]
    sticky_lifetime_referrers = chain_props["sticky_lifetime_referers"]["referrer_ids"]

    assert enforced_lifetime_referrer_id
    assert enforced_lifetime_referrer_id in sticky_lifetime_referrers

    ico_id = node.get_account(accounts[0].name)["id"]

    # except ico account
    for account in accounts[1:]:
        account_obj = node.get_account(account.name)

        assert account_obj
        life_time_referrer_id = account_obj["lifetime_referrer"]

        assert life_time_referrer_id
        assert enforced_lifetime_referrer_id == life_time_referrer_id, \
            "Lifetime referrer should be same as enforced referrer"

        referrer_id = account_obj["referrer"]

        if ltm_accounts:
            account_id = account_obj["id"]
            assert account_id == referrer_id, "Referrer should be account itself"
        else:
            assert ico_id == referrer_id, "Referrer should be ICO account"


def test_account_existing(testnet: PrivateTestnet):
    test_name = "ico1"

    account = testnet.trusted_node.get_account(test_name)
    assert account
    assert account["name"] == test_name

    acc_id = account["id"]
    assert acc_id
    balances = get_account_balances(testnet.trusted_node, acc_id)
    assert len(balances) == 0


def test_account_not_existing(testnet: PrivateTestnet):
    try:
        account = testnet.trusted_node.get_account("alice")
        assert False  # we expect exception
    except ObjectDoesNotExistsException as ex:
        assert True  # we expect exception


def test_regular_account_referral(testnet_with_accounts: PrivateTestnet):
    verify_referrals(testnet_with_accounts)


@pytest.mark.flaky(reruns=10)
def test_ltm_account_referral(testnet_with_ltm_accounts: PrivateTestnet):
    verify_referrals(testnet_with_ltm_accounts, True)

