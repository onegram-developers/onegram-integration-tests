import pytest


def check_permission(rules, account_name=None, account_id=None, account_type=None):
    print(f'rules: {rules}')
    for rule in rules:
        print(f'rule: {rule}')
        if "fee_payer_name" in rule:
            if rule["fee_payer_name"] == account_name:
                return rule["allowed"]
        elif "fee_payer_id" in rule:
            if rule["fee_payer_id"] == account_id:
                return rule["allowed"]
        elif "fee_payer_type" in rule:
            if rule["fee_payer_type"] == account_type:
                return rule["allowed"]
        else:
            return rule["allowed"]

    return False


@pytest.mark.skip(reason="under construction")
def test_transfer_permissions(testnet_with_accounts):
    node = testnet_with_accounts.trusted_node
    wallet = testnet_with_accounts.cli_wallet
    accounts = testnet_with_accounts.accounts

    params = node.get_global_properties()["parameters"]
    permissions = params["current_operations_permissions"]["parameters"]

    op_id = 0

    asset_options = {
        "max_supply": 100000,
        "market_fee_percent": 10,
        "max_market_fee": 1,
        "issuer_permissions": 0,
        "flags": 0,
        "core_exchange_rate": {
            "base": {
                "amount": 1,  # denominator
                "asset_id": "1.3.0"  # OGC
            },
            "quote": {
                "amount": 100,  # numerator
                "asset_id": "1.3.1"  # THIS asset
            }
        },
        "whitelist_authorities": [],
        "blacklist_authorities": [],
        "whitelist_markets": [],
        "blacklist_markets": [],
        "description": "Banana coin"
    }

    transaction = wallet.create_asset(accounts[1].name, "BNN", "2", asset_options, None, True)
    print(f"{transaction}")

    # # for account in accounts[:2]:
    #
    # account_from = ""
    # account_to = ""
    # amount = "1"
    # asset_symbol = "OGC"
    #
    # transaction = wallet.transfer(account_from, account_to, amount, asset_symbol, "", True)
    #
    #
    # op_id2, permission = permissions[op_id]
    # assert int(op_id) == int(op_id2)
    #
    # rules = permission["rules"]
    # # if check_permission(rules, account_name=account.name):








