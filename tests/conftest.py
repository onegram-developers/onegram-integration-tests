""" Common Test Fixtures """
import pytest
import typing
import os.path
from pytest_docker_compose import Container
from pytest_docker_compose import NetworkInfo
from time import sleep, time
from utils.rpc import WitnessNodeRPC, CliWalletRPC, ConnectionDoesNotExistsException, ObjectDoesNotExistsException
import utils.line_processors as line_processors
from tests import PrivateTestnet, Account
import tests.status_messages as status_messages
import random
import docker


pytest_plugins = ['docker_compose']

ico_account = Account("ico1", "5KQXNuNt5j6ef9b7H45th6t4w6bhpSv9rsMYMg8iwr9qGVPqUs1")


def teardown_testnet(t_net: PrivateTestnet):
    print("\ntearing down testnet...\n")

    if t_net.trusted_node is not None:
        t_net.trusted_node.disconnect()

    if t_net.cli_wallet is not None:
        t_net.cli_wallet.disconnect()


def running_in_docker():
    cgroup_path = '/proc/self/cgroup'
    if os.path.exists(cgroup_path):
        with open(cgroup_path, 'r') as procfile:
            for line in procfile:
                fields = line.strip().split('/')
                if 'docker' in fields:
                    return True
    return False


def docker_is_alive():
    d = docker.from_env()
    try:
        d.info()
        return True
    except Exception:
        return False


@pytest.fixture
def arg(request):
    return request.getfixturevalue(request.param)


def is_port_free(port):
    import socket
    try:
        tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp.bind(('', port))
        tcp.close()
        return True
    except Exception:
        return False


@pytest.fixture
def free_ports_fixture():
    wait_for_ports([8540, 11011, 22022])


@pytest.fixture
def setup(free_ports_fixture, docker_containers: typing.List[Container]):
    print('*********************** setup start ***********************')

    for container in sorted(docker_containers, key=lambda c: c.name):
        header = f"Logs from {container.name}:"
        print(header)
        print("=" * len(header))
        print(
            container.logs().decode("utf-8", errors="replace") or
            "(no logs)"
        )
        print()

    sleep(10)
    print('*********************** setup resumed ***********************')


@pytest.fixture
def dev_remote_rpc_url():
    return "wss://test-rpc.onegramcoin.net/rpc"


@pytest.fixture
def remote_rpc_url():
    return "ws://127.0.0.1"  #"ws://159.69.112.106"


@pytest.fixture
def remote_node_port():
    return 8088


@pytest.fixture
def remote_wallet_port():
    return 8089


@pytest.fixture(scope="session")
def local_rpc_url():
    if running_in_docker():
        return "ws://docker"
    else:
        return "ws://localhost"


@pytest.fixture(scope="session")
def local_node_port():
    return 8090


@pytest.fixture(scope="session")
def local_wallet_port():
    return 8091


@pytest.fixture(scope="session")
def image_name(request):
    environment = request.config.option.target_environment
    if (environment is not None) and (environment == "MAINNET"):
        return "onegram/onegram-core"
    else:  # environment == TESTNET
        return "onegram-test"


_container = None
@pytest.fixture
def shared_container(image_name):
    global _container

    if _container is None:
        d = docker.from_env()
        _container = d.containers.run(image_name,
                                      command="bash",
                                      user='root',
                                      detach=True,
                                      tty=True)

        assert _container is not None
        assert _container.status == 'created'

    _container.start()
    yield _container
    _container.stop()


def wait_for_event(msg, timeout=120, step=1):

    def _wait_for_event(event):

        def __wait_for_event(*args, **kwargs):
            print(f'Waiting for: {msg}')

            start = time()

            while True:
                elapsed_seconds = time() - start
                if elapsed_seconds >= timeout:
                    raise RuntimeError(f'Unable to {msg} within {timeout} seconds.')
                elif event(*args, **kwargs):
                    break
                else:
                    print(f'waiting {elapsed_seconds:.0f}s')

                sleep(step)
        return __wait_for_event
    return _wait_for_event


@wait_for_event("ports to be free", 600, 5)
def wait_for_ports(ports):
    for port in ports:
        if not is_port_free(port):
            return False
    return True


@wait_for_event("start rpc service", 900, 5)
def wait_for_rpc_service(rpc):
    return rpc.is_alive()


@wait_for_event("prepare testnet")
def wait_for_current_block(rpc):
    try:
        current_block = rpc.get_head_block()

        print(f'current block: {current_block}')
    except (ConnectionDoesNotExistsException, ObjectDoesNotExistsException) as e:
        return False
    else:
        return True


@wait_for_event("get next block", 200, 5)
def wait_for_next_block(rpc, current_block_num):
    next_block_num = rpc.get_head_block_num()
    return next_block_num > current_block_num


# wait max 6 hours for sync with step 30secs
@wait_for_event("synchronization", 6*3600, 30)
def wait_for_node_sync(docker_container, line_processor):
    # for now this ('docker_container') should be enough, we can design interface for this in the future
    last_line = docker_container.logs(tail=1).decode('ascii')
    res = line_processor.process_line(last_line)

    return res.break_loop


@pytest.fixture
def testnet(free_ports_fixture, docker_network_info: typing.Dict[str, typing.List[NetworkInfo]], request) -> PrivateTestnet:
    is_in_docker = running_in_docker()

    if is_in_docker:
        print("Running tests in docker container.")

    rpc_network_info = {}
    for key, value in docker_network_info.items():
        key_lower = key.lower()
        if len(value) > 0:
            network_info = value[0]
            if is_in_docker and (network_info.hostname == "localhost"):
                network_info.hostname = "docker"
            if 'rpc' in key_lower:
                rpc_network_info['rpc'] = network_info
            if 'wallet' in key_lower:
                rpc_network_info['wallet'] = network_info

    assert 'rpc' in rpc_network_info, 'RPC Node Not Found'
    assert 'wallet' in rpc_network_info, 'CLI Wallet Node Not Found'

    # TODO: change Testnet and add other witness nodes to it
    # witness_nodes_rpc = {key: WitnessNodeRPC.from_network_info(value) for key, value in docker_network_info.items() if 'witness' in key}

    rpc_node_info = rpc_network_info['rpc']
    node_rpc = WitnessNodeRPC.from_network_info(rpc_node_info)

    wallet_node_info = rpc_network_info['wallet']
    wallet_rpc = CliWalletRPC.from_network_info(wallet_node_info)

    print(f'node url: {node_rpc.rpc.node_url}')
    print(f'wallet url: {wallet_rpc.rpc.node_url}')

    wait_for_rpc_service(node_rpc)
    wait_for_rpc_service(wallet_rpc)
    wait_for_current_block(node_rpc)

    return PrivateTestnet(node_rpc, wallet_rpc, [])


@pytest.fixture(scope="session")
def node_rpc_connection():
    def create_rpc_connection(url='wss://rpc.onegramcoin.net/rpc'):
        node_rpc = WitnessNodeRPC.from_url(url)
        wait_for_rpc_service(node_rpc)
        return node_rpc

    return create_rpc_connection


@pytest.fixture(scope="session")
def wallet_rpc_connection():
    def create_rpc_connection(url='ws://127.0.0.1:8091'):
        wallet_rpc = CliWalletRPC.from_url(url)
        wait_for_rpc_service(wallet_rpc)
        return wallet_rpc

    return create_rpc_connection


def _local_testnet(node_rpc_connection, wallet_rpc_connection, image_name, local_rpc_url, local_node_port, local_wallet_port):
    docker_client = docker.from_env()

    # TODO: make this fixture ?
    plugins = [
        "account_archive",
        "account_history"
    ]

    run_witness_command = (f"/usr/local/bin/witness_node"
                           f" --data-dir=/home/onegram/witness_test"
                           f" --rpc-endpoint=0.0.0.0:{local_node_port}"
                           f" --plugins=\"{' '.join(plugins)}\"")

    print(f"command: {run_witness_command}")

    # TODO: maybe we should use shared_container fixture (+ exec_run) instead of this ...
    node_container = docker_client.containers.run(
        image=image_name,
        command=run_witness_command,
        detach=True,
        auto_remove=True,  # TODO: check this feature
        tty=True,
        user='root',
        ports={f"{local_node_port}/tcp": local_node_port}  # map to the same port
    )

    # wait for service first, it should be available even if node isn't fully synced yet
    node_rpc = node_rpc_connection(f"{local_rpc_url}:{local_node_port}")

    # wait for synchronization process to finish - this might take a while
    ln_proc = line_processors.BlockSyncProcessor()
    wait_for_node_sync(node_container, ln_proc)

    wallet_rpc = None
    if wallet_rpc_connection is not None:
        run_wallet_command = (f"/usr/local/bin/cli_wallet"
                              f" --server-rpc-endpoint=ws://0.0.0.0:{local_node_port}"
                              f" --rpc-endpoint=0.0.0.0:{local_wallet_port}")

        docker_client.containers.run(
            image=image_name,
            command=run_wallet_command,
            detach=True,
            auto_remove=True,
            tty=True,
            user='root',
            ports={f"{local_wallet_port}/tcp": local_wallet_port}  # map to the same port
        )

        wallet_rpc = wallet_rpc_connection(f"{local_rpc_url}:{local_wallet_port}")
        wait_for_rpc_service(wallet_rpc)

    return PrivateTestnet(node_rpc, wallet_rpc, []), node_container


# TODO: rename
# we use same instance of fixture for all test in the session to reduce execution time
@pytest.fixture(scope="session")
def local_testnet(node_rpc_connection, wallet_rpc_connection, image_name, local_rpc_url, local_node_port, local_wallet_port):
    testnet, container = _local_testnet(node_rpc_connection, wallet_rpc_connection, image_name, local_rpc_url, local_node_port, local_wallet_port)
    yield testnet
    container.stop()

# we use same instance of fixture for all test in the session to reduce execution time
@pytest.fixture(scope="session")
def local_testnet_no_wallet(node_rpc_connection, image_name, local_rpc_url, local_node_port):
    testnet, container = _local_testnet(node_rpc_connection, None, image_name, local_rpc_url, local_node_port, None)
    yield testnet
    container.stop()


# TODO: rename
@pytest.fixture
def remote_testnet(node_rpc_connection, wallet_rpc_connection, remote_rpc_url, remote_node_port, remote_wallet_port):
    node_rpc = node_rpc_connection(f"{remote_rpc_url}:{remote_node_port}")
    wallet_rpc = wallet_rpc_connection(f"{remote_rpc_url}:{remote_wallet_port}")

    wait_for_rpc_service(node_rpc)
    wait_for_rpc_service(wallet_rpc)

    return PrivateTestnet(node_rpc, wallet_rpc, [])


# TODO: rename
@pytest.fixture
def remote_testnet_no_wallet(node_rpc_connection, remote_rpc_url, remote_node_port):
    node_rpc = node_rpc_connection(f"{remote_rpc_url}:{remote_node_port}")

    wait_for_rpc_service(node_rpc)

    return PrivateTestnet(node_rpc, None, [])


# TODO: make wallet version
@pytest.fixture
def dev_remote_testnet_no_wallet(node_rpc_connection, dev_remote_rpc_url):
    node_rpc = node_rpc_connection(dev_remote_rpc_url)
    return PrivateTestnet(node_rpc, None, [])


def import_account_to_wallet(wallet, account):
    assert not wallet.is_locked(), "Wallet should be unlocked"

    r = wallet.import_key(account.name, account.wif_key)
    assert r, "Importing account failed"

    wif_keys = [account.wif_key]
    transactions = wallet.import_balance(account.name, wif_keys, True)

    assert len(transactions) == len(wif_keys)


@pytest.fixture
def testnet_with_ico_account(testnet):
    secret = "supersecret"

    wallet = testnet.cli_wallet

    if wallet.is_new():
        wallet.set_password(secret)

    if wallet.is_locked():
        wallet.unlock(secret)

    import_account_to_wallet(wallet, ico_account)

    testnet.accounts.append(ico_account)
    yield testnet
    teardown_testnet(testnet)


def create_account(wallet, account_name):
    ico = ico_account

    registrar = ico.name
    referrer = ico.name

    brain_key = wallet.suggest_brain_key()
    assert brain_key is not None

    brain_priv_key = brain_key["brain_priv_key"]
    transaction = wallet.create_account_with_brain_key(brain_priv_key, account_name, registrar, referrer, True)
    assert transaction, "Creating account failed!"

    wif_key = brain_key["wif_priv_key"]

    return Account(account_name, wif_key)


@pytest.fixture
def testnet_with_accounts(testnet_with_ico_account):
    result = testnet_with_ico_account

    wallet = result.cli_wallet

    account_count = 15
    for i in range(0, account_count):
        account_name = "my-user-{0:04d}".format(i)
        account = create_account(wallet, account_name)
        result.accounts.append(account)

        balance_amount = random.randint(1000, 2000)
        r = wallet.transfer(ico_account.name, account.name, balance_amount, "OGC", "ico", True)
        assert r, "Transfer failed"

        r = wallet.import_key(account.name, account.wif_key)
        assert r, "Importing account failed"

    node = result.trusted_node

    current_block_num = node.get_head_block_num()
    wait_for_rpc_service(node)
    wait_for_next_block(node, current_block_num)

    yield result
    teardown_testnet(result)


@pytest.fixture
def testnet_with_ltm_accounts(testnet_with_accounts):
    result = testnet_with_accounts

    wallet = result.cli_wallet

    # except ico account, it is already ltm
    for account in result.accounts[1:]:
        r = wallet.upgrade_account(account.name, True)
        assert r, "Upgrade failed"

    yield result
    teardown_testnet(result)


def pytest_addoption(parser):
    parser.addoption('--witness-binary', action="store", default="witness_node")
    parser.addoption('--data-dir', action="store", default="/home/onegram/witness_node_data_dir")
    parser.addoption('--target-environment', action="store", default="TESTNET")   # TODO: change docker-compose in case of MAINNET


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_terminal_summary(terminalreporter, exitstatus):
    outcome = yield
    outcome.get_result()

    stats = terminalreporter.stats
    # keys = "failed passed skipped deselected xfailed xpassed warnings error".split()

    fail_keys = "failed xfailed error warning".split()
    success_keys = "passed xpassed".split()
    skip_keys = "skipped deselected".split()

    fail_count = 0
    success_count = 0
    skip_count = 0
    for key in stats.keys():
        if key:
            if key in fail_keys:
                fail_count += 1
            elif key in success_keys:
                success_count += 1
            elif key in skip_keys:
                skip_count += 1

    if (fail_count + success_count) > 0:
        writer = terminalreporter.writer
        if exitstatus == 0:
            markup = {"bold": True, "yellow": True}
            writer.write(status_messages.SUCCESS_MESSAGE, **markup)
        else:
            markup = {"bold": True, "red": True}
            writer.write(status_messages.FAILURE_MESSAGE, **markup)
