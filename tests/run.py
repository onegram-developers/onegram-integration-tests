import pytest
import argparse

test_groups = {
    "common_tests": [
        "test_fixtures.py",
    ],
    "sync_tests": [
        "test_blockchain_sync.py",
    ],
    "blockchain_tests": [
        "test_account.py",
        "test_asset.py",
        "test_fees.py",
        "test_permissions.py",
        "test_wallet.py",
    ],
    "api_tests": [
        "test_archive_plugin.py",
        "test_database_api_globals.py",
        "test_database_api_blocks.py",
        "test_database_api_accounts.py",
        "test_database_api_assets.py",
        "test_database_api_government.py",
        "test_history_api.py",
    ]
}

keywords = [k for k in test_groups.keys()]


def test_group_names(gs):
    for g in gs:
        if g not in keywords:
            return False
    return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run specific test group")
    parser.add_argument("--silent", const=True, default=False, action='store_const', help='do not print test output (default: False)')
    parser.add_argument("test_groups", metavar="GROUP", nargs='*', help="possible values: " + ', '.join(keywords))
    args = parser.parse_args()

    groups = args.test_groups
    test_all = (groups is None) or (len(groups) == 0)

    if not test_group_names(groups):
        print("\n----INVALID GROUP NAME----\n")
        parser.print_help()
    else:
        test_files = []
        for group_name, tests in test_groups.items():
            if test_all or (group_name in groups):
                test_files += tests

        print(test_files)

        test_args = []
        if not args.silent:
            test_args += ["-s"]

        exit(pytest.main(test_args + test_files))





