import time
import pytest
import random
from datetime import datetime, timedelta

# ===== ARCHIVE API ====
#
# query_result get_archived_operations(
#     size_t last,
#     size_t count,
#     flat_set<int> operation_id_filter = flat_set<int>()
# );
#
# query_result get_archived_operations_by_time(
#     time_point_sec inclusive_from,
#     time_point_sec exclusive_until,
#     size_t skip_count,
#     flat_set<int>operation_id_filter = flat_set<int>()
# );
#
# query_result get_archived_account_operations(
#     const string account_id_or_name,
#     size_t last,
#     size_t count,
#     flat_set<int> operation_id_filter = flat_se t <in t >()
# );
#
# query_result get_archived_account_operations_by_time(
#     const string account_id_or_name,
#     time_point_sec inclusive_from,
#     time_point_sec exclusive_until,
#     size_t skip_count,
#     flat_set<int> operation_id_filter = flat_set<int>()
# );
#
# size_t get_archived_account_operation_count(const string account_id_or_name);
#
#
#  ==== HISTORY API ====
#
#  vector<operation_history_object> get_account_history(
#      string account_id_or_name,
#      operation_history_id_type stop = operation_history_id_type(),
#      unsigned limit = 100,
#      operation_history_id_type start = operation_history_id_type()
#  )
#
#   history_operation_detail get_account_history_by_operations(
#       string account_id_or_name,
#       vector<uint16_t> operation_types,
#       uint32_t start,
#       unsigned limit
#   )
#
#  vector<operation_history_object> get_account_history_operations(
#       string account_id_or_name,
#       int operation_id,
#       operation_history_id_type start = operation_history_id_type(),
#       operation_history_id_type stop = operation_history_id_type(),
#       unsigned limit = 100
#  )
#
#  vector<operation_history_object> get_relative_account_history(
#       string account_id_or_name,
#       uint32_t stop = 0,
#       unsigned limit = 100,
#       uint32_t start = 0)
#
#  vector<operation_history_object> get_last_operations_history(unsigned limit = OperationHistoryObjectsLimit);


max_op_count = 10000


def get_query_limit(archive_api):
    arch_params = archive_api.get_archive_api_parameters()
    return arch_params["QueryResultLimit"]


def int_to_account_id(number):
    return f"1.2.{number}"


def get_archived_operation_count(rpc):
    dyn_props = rpc.get_dynamic_global_properties()
    last_op_id = dyn_props["last_operation_id"]
    last_op_index = int(last_op_id[5:])  # '1.11.xxx'
    return last_op_index + 1


def time2str(t):
    return f"{t:%Y-%m-%dT%H:%M:%S}"


def str2time(s):
    return datetime.strptime(s, "%Y-%m-%dT%H:%M:%S")


def generate_random_time_interval(from_time, to_time):
    from_t = str2time(from_time)
    to_t = str2time(to_time)

    start = random.randint(from_t.timestamp(), to_t.timestamp())
    start_t = datetime.fromtimestamp(start)

    max_duration = (to_t - start_t)
    duration = random.randint(0, max_duration.total_seconds())

    end_t = start_t + timedelta(seconds=duration)

    return start_t, end_t


@pytest.mark.skip(reason="helper function")
def test_time_query(node, from_t, to_t, account=None, op_filter=[]):

    from_time = time2str(from_t)
    to_time = time2str(to_t)

    all_processed = 0
    while all_processed < 300:
        r = {}
        if account:
            r = node.archive_api.get_archived_account_operations_by_time(account, from_time, to_time, all_processed, op_filter)
        else:
            r = node.archive_api.get_archived_operations_by_time(from_time, to_time, all_processed, op_filter)

        for op in r["operations"]:
            block_num = op["block_num"]
            trx_in_block = op["trx_in_block"]
            op_in_trx = op["op_in_trx"]

            block = node.get_block(block_num)

            block_time = block["timestamp"]
            blockt = str2time(block_time)

            assert blockt >= from_t
            assert blockt < to_t

            transactions = block["transactions"]

            assert trx_in_block < len(transactions)

            trx = transactions[trx_in_block]
            operations = trx["operations"]

            assert op_in_trx < len(operations)

            assert operations[op_in_trx] == op["op"]

            op_id = op["op"][0]
            assert (not op_filter) or (op_id in op_filter), "operation wasn't requested"

        num_processed = r["num_processed"]
        all_processed += num_processed
        if num_processed < 100:
            break

    return all_processed


def test_archive_get_account_ops_perf(local_testnet_no_wallet):
    global max_op_count

    node = local_testnet_no_wallet.trusted_node

    limit = get_query_limit(node.archive_api)

    account = int_to_account_id(1000)

    archive_api = node.archive_api
    orig_op_count = archive_api.get_archived_account_operation_count(account)

    print(f"Account op count: {orig_op_count}")

    start_t = time.time()

    query_count = 0
    op_count = min(orig_op_count, max_op_count)
    remaining_op_count = op_count
    account_ops = []
    while remaining_op_count > 0:
        start = orig_op_count - (op_count - remaining_op_count) - 1
        q_limit = min(remaining_op_count, limit)
        result = archive_api.get_archived_account_operations(account, start, q_limit, [])
        num_processed = result["num_processed"]

        assert remaining_op_count >= len(result["operations"])
        assert num_processed > 0

        remaining_op_count -= num_processed
        account_ops.extend(result["operations"])
        query_count += 1

    end_t = time.time()

    duration = end_t - start_t
    print(f"get {op_count} archived account operations in: {duration}s")

    # we wanna keep average time per query reasonably fast, so it doesn't bother user
    assert (duration / query_count) < 1.0

    assert len(account_ops) == op_count


def test_archive_get_all_ops_perf(local_testnet_no_wallet):
    global max_op_count

    node = local_testnet_no_wallet.trusted_node

    archive_api = node.archive_api

    limit = get_query_limit(archive_api)

    orig_all_op_count = get_archived_operation_count(node)

    print(f"All op count: {orig_all_op_count}")

    start_t = time.time()

    all_op_count = min(orig_all_op_count, max_op_count)

    query_count = 0
    all_ops = []
    remaining_op_count = all_op_count
    while remaining_op_count > 0:
        start = orig_all_op_count - (all_op_count - remaining_op_count) - 1
        q_limit = min(remaining_op_count, limit)
        result = archive_api.get_archived_operations(start, q_limit, [])
        num_processed = result["num_processed"]

        assert remaining_op_count >= len(result["operations"])
        assert num_processed > 0

        remaining_op_count -= num_processed
        all_ops.extend(result["operations"])
        query_count += 1

    end_t = time.time()

    duration = end_t - start_t
    print(f"get {all_op_count} archived operations in: {duration}s")

    assert (duration / query_count) < 1.0

    assert len(all_ops) == all_op_count


def test_archive_against_history(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    limit = get_query_limit(node.archive_api)

    assert limit <= 100  # 100 is history limit

    all_hist_ops = node.history_api.get_last_operations_history(limit)

    ops_count = get_archived_operation_count(node)
    q_limit = min(ops_count, limit)
    result = node.archive_api.get_archived_operations(max(ops_count - 1, 0), q_limit, [])

    assert result["num_processed"] == limit

    all_arch_ops = result["operations"]

    assert len(all_hist_ops) == limit
    assert len(all_arch_ops) == limit

    for i in range(0, limit):
        assert all_arch_ops[i] == all_hist_ops[i], "archive api should return same results as history api"

    account_count = 100
    for i in range(0, account_count):
        account_id = int_to_account_id(i)
        result_hist = node.history_api.get_relative_account_history(account_id, 0, limit, 0)

        ops_count = node.archive_api.get_archived_account_operation_count(account_id)
        q_limit = min(ops_count, limit)
        result_arch = node.archive_api.get_archived_account_operations(account_id, max(ops_count - 1, 0), q_limit, [])

        hist_op_count = len(result_hist)
        assert hist_op_count <= limit
        assert result_arch["num_processed"] == hist_op_count

        result_arch = result_arch["operations"]
        assert len(result_arch) == hist_op_count

        for j in range(0, hist_op_count):
            assert result_hist[j] == result_arch[j], "archive api should return same results as history api"


@pytest.mark.skip(reason="helper function")
def test_filter_of_all_archived_ops(node, op_filter):
    global max_op_count

    orig_ops_count = get_archived_operation_count(node)
    ops_count = min(orig_ops_count, max_op_count)
    limit = get_query_limit(node.archive_api)

    remaining_op_count = ops_count
    while remaining_op_count > 0:
        start = orig_ops_count - (ops_count - remaining_op_count) - 1
        q_limit = min(remaining_op_count, limit)
        result = node.archive_api.get_archived_operations(start, q_limit, op_filter)
        num_processed = result["num_processed"]

        assert remaining_op_count >= len(result["operations"])
        assert num_processed > 0

        remaining_op_count -= num_processed

        for op in result["operations"]:
            op_type = op["op"][0]
            assert op_type in op_filter


@pytest.mark.skip(reason="helper function")
def test_filter_of_all_archived_ops_by_time(node, op_filter):
    first_block = node.get_block(1)
    head_block = node.get_head_block()

    first_block_time = first_block["timestamp"]
    last_block_time = head_block["timestamp"]

    iteration_count = 100
    for i in range(0, iteration_count):
        from_t, to_t = generate_random_time_interval(first_block_time, last_block_time)
        test_time_query(node, from_t, to_t, None, op_filter)


@pytest.mark.skip(reason="helper function")
def test_filter_of_account_archived_ops(node, op_filter):

    limit = get_query_limit(node.archive_api)
    account_count = 100
    for i in range(0, account_count):
        account_index = random.randint(200, 1000)

        account_id = int_to_account_id(account_index)

        ops_count = node.archive_api.get_archived_account_operation_count(account_id)

        assert ops_count > 0

        q_limit = min(ops_count, limit)
        result = node.archive_api.get_archived_account_operations(account_id, ops_count - 1, q_limit, op_filter)

        for op in result["operations"]:
            op_type = op["op"][0]
            assert op_type in op_filter


@pytest.mark.skip(reason="helper function")
def test_filter_of_account_archived_ops_by_time(node, op_filter):
    first_block = node.get_block(1)
    head_block = node.get_head_block()

    first_block_time = first_block["timestamp"]
    last_block_time = head_block["timestamp"]

    iteration_count = 10
    account_count = 20
    for i in range(0, iteration_count):
        from_t, to_t = generate_random_time_interval(first_block_time, last_block_time)
        for j in range(0, account_count):
            account_index = random.randint(100, 1000)

            account_id = int_to_account_id(account_index)
            ops_count = node.archive_api.get_archived_account_operation_count(account_id)
            assert ops_count > 0

            test_time_query(node, from_t, to_t, account_id, op_filter)


@pytest.mark.skip(reason="helper function")
def test_filter(node, op_filter):
    test_filter_of_all_archived_ops(node, op_filter)
    test_filter_of_account_archived_ops(node, op_filter)
    test_filter_of_all_archived_ops_by_time(node, op_filter)
    test_filter_of_account_archived_ops_by_time(node, op_filter)


def test_archive_filters(local_testnet_no_wallet):
    global max_op_count

    node = local_testnet_no_wallet.trusted_node

    test_filter(node, [0])  # transfer

    test_filter(node, [5])  # create account

    test_filter(node, [0, 5])

    test_filter(node, [36])  # assert_operation


def test_archive_by_time_query(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    from_time = "2019-1-9T00:00:00"
    to_time = "2019-1-9T17:00:00"

    from_t = str2time(from_time)
    to_t = str2time(to_time)

    r = test_time_query(node, from_t, to_t)
    assert r == 18

    first_block = node.get_block(1)
    head_block = node.get_head_block()

    first_block_time = first_block["timestamp"]
    last_block_time = head_block["timestamp"]

    iteration_count = 100
    for i in range(0, iteration_count):
        from_t, to_t = generate_random_time_interval(first_block_time, last_block_time)
        test_time_query(node, from_t, to_t)


def test_archive_time_query_out_of_bounds(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    first_block = node.get_block(1)
    head_block = node.get_head_block()

    first_block_time = first_block["timestamp"]
    last_block_time = head_block["timestamp"]

    from_t, to_t = generate_random_time_interval(first_block_time, last_block_time)

    before_first_t = str2time(first_block_time) - timedelta(days=1)
    after_last_t = str2time(last_block_time) + timedelta(days=1)

    # case when start time is before first block
    test_time_query(node, before_first_t, to_t)

    # case when end time is after last block
    test_time_query(node, from_t, after_last_t)

    # case when start time & end time is out of bounds
    test_time_query(node, before_first_t, after_last_t)

    # case when start time & end time is before first
    test_time_query(node, before_first_t - timedelta(days=1), before_first_t)

    # case when start time & end time is after last
    test_time_query(node, after_last_t, after_last_t + timedelta(days=1))


def test_archive_time_query_edge_cases(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    head_block_num = node.get_head_block_num()

    first_block = node.get_block(1)
    head_block = node.get_head_block()

    first_block_time = first_block["timestamp"]
    last_block_time = head_block["timestamp"]

    it_count = 100
    for i in range(0, it_count):
        block_num = random.randint(1, head_block_num)
        block = node.get_block(block_num)
        block_time = block["timestamp"]

        from_t, to_t = generate_random_time_interval(block_time, last_block_time)
        test_time_query(node, from_t, to_t)

        from_t, to_t = generate_random_time_interval(first_block_time, block_time)
        test_time_query(node, from_t, to_t)


