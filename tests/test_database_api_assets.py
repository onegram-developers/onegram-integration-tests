import random
import utils

# https://dev.bitshares.works/en/master/api/blockchain_api/database.html#assets
# https://dev.bitshares.works/en/master/api/blockchain_api/database.html#balances


# Balances
# get_account_balances
# get_named_account_balances
# get_balance_objects
# get_vested_balances
# get_vesting_balances

# Assets
# get_assets
# list_assets
# lookup_asset_symbols

def list_object_ids(node, protocol_id, space_id, maxim=200):
    result = []
    for i in range(maxim):
        object_id = f"{protocol_id}.{space_id}.{i}"
        try:
            node.get_object(object_id)
            result.append(object_id)
        except utils.rpc.ObjectDoesNotExistsException as e:
            break

    return result


def list_asset_ids(node, maxim=100):
    return list_object_ids(node, 1, 3, maxim)


def list_vesting_balance_ids(node):
    return list_object_ids(node, 1, 13)


def get_random_account_ids(node, count):
    max_id = node.get_account_count() - 1
    ids = [f"1.2.{random.randint(0, max_id)}" for j in range(count)]
    return ids


def get_random_asset_ids(node, count):
    ids = list_asset_ids(node)
    return random.sample(ids, count)


def test_get_account_balances(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    ids = get_random_account_ids(node, 10)
    all_asset_ids = list_asset_ids(node)

    for acc_id in ids:
        for asset_count in range(1, 6):
            asset_ids = random.sample(all_asset_ids, asset_count)
            balances = node.get_account_balances(acc_id, asset_ids)
            assert len(balances) == len(asset_ids)
            for balance in balances:
                assert int(balance['amount']) >= 0
                assert balance['asset_id'] in asset_ids


def test_get_named_account_balances(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    ids = get_random_account_ids(node, 10)
    all_asset_ids = list_asset_ids(node)

    for acc_id in ids:
        for asset_count in range(1, 6):
            asset_ids = random.sample(all_asset_ids, asset_count)
            account = node.get_account(acc_id)
            balances_id = node.get_account_balances(acc_id, asset_ids)
            balances_name = node.get_named_account_balances(account['name'], asset_ids)
            assert balances_id == balances_name


def test_get_balance_objects(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    expected_keys = {'id', 'owner', 'balance', 'last_claim_date'}

    assert node.get_balance_objects([]) == []

    obj = node.get_object("1.15.1")
    balance_objs = node.get_balance_objects([obj['owner']])

    for key in expected_keys:
        assert key in obj

    assert len(balance_objs) == 1
    assert obj == balance_objs[0]


def test_get_vested_balances(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node
    assert node.get_balance_objects([]) == []

    vested_balances = node.get_vested_balances(["1.15.1"])
    balance_object = node.get_object("1.15.1")

    assert 'amount' in vested_balances[0]
    assert 'asset_id' in vested_balances[0]

    assert vested_balances[0] == balance_object['balance']


def test_get_vesting_balances(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    expected_keys = ['id', 'owner', 'balance', 'policy']

    ids = list_vesting_balance_ids(node)

    balance_obj = node.get_object(ids[0])
    vesting_balances = node.get_vesting_balances(balance_obj['owner'])

    for key in expected_keys:
        for balance in vesting_balances:
            assert key in balance

    for balance_id in ids:
        balance_obj = node.get_object(balance_id)
        vesting_balance = node.get_vesting_balances(balance_obj['owner'])
        assert balance_obj in vesting_balance


def test_get_assets(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    expected_keys = {
        'id',
        'symbol',
        'precision',
        'issuer',
        'options',
        'dynamic_asset_data_id',
    }

    optional_keys = {
        'bitasset_data_id',
        'buyback_account',
    }

    assert node.get_assets([]) == []

    all_asset_ids = list_asset_ids(node)
    it_count = len(all_asset_ids)
    for i in range(1, it_count + 1):
        asset_ids = random.sample(all_asset_ids, i)
        assets = node.get_assets(asset_ids)

        assert len(asset_ids) == len(assets)

        for j in range(0, len(asset_ids)):
            assert assets[j]['id'] == asset_ids[j]

        for asset in assets:
            for key in expected_keys:
                assert key in asset
            for key in optional_keys:
                assert key not in asset or asset[key] is not None


def test_list_assets(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    limit = 50
    expected_ids = list_asset_ids(node, limit)
    asset_list = node.list_assets("", 50)

    assert len(expected_ids) == len(asset_list)
    for asset in asset_list:
        assert asset['id'] in expected_ids

    for i in range(len(asset_list)):
        assert len(node.list_assets("", i)) <= i

    it_count = 5
    for i in range(it_count):
        asset_id = random.sample(expected_ids, 1)[0]
        asset = node.get_asset(asset_id)
        a_list = node.list_assets(asset['symbol'], 1)
        assert asset == a_list[0]


def test_lookup_asset_symbols(local_testnet_no_wallet):
    def maybe_symbol(nd, asset_id, get_symbol):
        if get_symbol:
            return nd.get_asset(asset_id)['symbol']
        else:
            return asset_id

    node = local_testnet_no_wallet.trusted_node

    # test non existing asset
    assets = node.lookup_asset_symbols([""])
    assert len(assets) == 1
    assert assets[0] is None

    all_asset_ids = list_asset_ids(node)
    for i in range(len(all_asset_ids)):
        asset_ids = random.sample(all_asset_ids, i)
        bools = [random.choice([False, True]) for i in asset_ids]
        ids_and_symbols = list(map(lambda asset_id, is_symbol: maybe_symbol(node, asset_id, is_symbol), asset_ids, bools))
        asset_symbols = node.lookup_asset_symbols(ids_and_symbols)
        assert len(asset_symbols) == len(ids_and_symbols)
        assert None not in asset_symbols


