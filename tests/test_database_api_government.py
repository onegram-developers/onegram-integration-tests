import random

# Witnesses
# get_witnesses
# get_witness_by_account
# lookup_witness_accounts
# get_witness_count
#
# Committee members
# get_committee_members
# get_committee_member_by_account
# lookup_committee_member_accounts
#
# Workers
# get_workers_by_account
#
# Votes
# lookup_vote_ids
# Proposed Transactions
# get_proposed_transactions
#
# https://dev.bitshares.works/en/master/api/blockchain_api/database.html#witnesses


def list_witness_ids(node):
    count = node.get_witness_count()
    return [f"1.6.{i}" for i in range(1, count + 1)]


def list_witness_accounts_ids(node):
    w_ids = list_witness_ids(node)
    witnesses = node.get_witnesses(w_ids)
    return list(map(lambda w: w['witness_account'], witnesses))


def list_committee_ids(node):
    count = node.get_committee_count()
    return [f"1.5.{i}" for i in range(count)]
    witnesses = node.get_witnesses(w_ids)


def list_committee_accounts_ids(node):
    c_ids = list_committee_ids(node)
    members = node.get_committee_members(c_ids)
    return list(map(lambda m: m['committee_member_account'], members))


def get_random_witness_ids(node, count):
    w_ids = list_witness_ids(node)
    return random.sample(w_ids, count)


def get_random_committee_ids(node, count):
    c_ids = list_committee_ids(node)
    return random.sample(c_ids, count)


def test_get_witnesses(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    expected_keys = {
        'id', 'witness_account', 'last_aslot', 'signing_key',
        'pay_vb', 'vote_id', 'total_votes', 'url', 'total_missed', 'last_confirmed_block_num',
    }

    w_count = node.get_witness_count()
    for i in range(w_count):
        w_ids = get_random_witness_ids(node, i)
        witnesses = node.get_witnesses(w_ids)
        assert len(w_ids) == len(witnesses)
        assert None not in witnesses
        for witness in witnesses:
            assert witness['id'] in w_ids
            for key in expected_keys:
                assert key in witness


def test_get_witness_by_account(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node
    witness_accounts = list_witness_accounts_ids(node)

    for acc_id in witness_accounts:
        witness = node.get_witness_by_account(acc_id)
        assert witness is not None
        assert witness['witness_account'] == acc_id
        assert witness == node.get_witnesses([witness['id']])[0]

    max_acc_id = node.get_account_count() - 1
    while True:
        not_witness_account = f"1.2.{random.randint(0, max_acc_id)}"
        if not_witness_account not in witness_accounts:
            break

    assert node.get_witness_by_account(not_witness_account) is None


def test_lookup_witness_accounts(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    limit = 1000
    witness_list = node.lookup_witness_accounts("", limit)
    assert len(witness_list) <= limit
    assert len(witness_list) <= node.get_witness_count()

    witness_count = node.get_witness_count()
    if witness_count <= limit:
        acc_ids = list_witness_accounts_ids(node)
        acc_names = [node.get_account(acc_id)['name'] for acc_id in acc_ids]
        for witness in witness_list:
            assert witness[0] in acc_names
            w_instance = int(witness[1].split('.')[2])
            assert w_instance <= witness_count
            assert w_instance > 0


def test_get_witness_count(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node
    props = node.get_chain_properties()
    min_witness_count = props['immutable_parameters']['min_witness_count']

    cfg = node.database_api.get_config()
    max_witness_count = cfg['GRAPHENE_DEFAULT_MAX_WITNESSES']

    count = node.get_witness_count()
    assert count >= min_witness_count
    assert count <= max_witness_count


def test_get_committee_members(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    expected_keys = {'id', 'committee_member_account', 'vote_id', 'total_votes', 'url'}

    assert node.get_committee_members([]) == []
    c_count = node.get_committee_count()
    invalid_committee_id = f"1.5.{c_count + 100}"
    assert node.get_committee_members([invalid_committee_id]) == [None]

    for i in range(c_count):
        ids = get_random_committee_ids(node, i + 1)
        members = node.get_committee_members(ids)
        for member in members:
            assert member['id'] in ids
            for key in expected_keys:
                assert key in member


def test_get_committee_member_by_account(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node
    committee_accounts = list_committee_accounts_ids(node)

    for acc_id in committee_accounts:
        member = node.get_committee_member_by_account(acc_id)
        assert member is not None
        assert member['committee_member_account'] == acc_id
        assert member == node.get_committee_members([member['id']])[0]

    max_acc_id = node.get_account_count() - 1
    while True:
        not_committee_account = f"1.2.{random.randint(0, max_acc_id)}"
        if not_committee_account not in committee_accounts:
            break

    assert node.get_committee_member_by_account(not_committee_account) is None


def test_lookup_committee_member_accounts(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node
    limit = 1000

    assert node.lookup_committee_member_accounts("zzz", limit) == []

    committee_list = node.lookup_committee_member_accounts("", limit)
    assert len(committee_list) <= limit
    assert len(committee_list) <= node.get_committee_count()

    committee_count = node.get_committee_count()
    if committee_count <= limit:
        acc_ids = list_committee_accounts_ids(node)
        acc_names = [node.get_account(acc_id)['name'] for acc_id in acc_ids]
        for members in committee_list:
            assert members[0] in acc_names
            m_instance = int(members[1].split('.')[2])
            assert m_instance < committee_count
            assert m_instance >= 0


def test_get_committee_count(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node
    props = node.get_chain_properties()
    min_committee_count = props['immutable_parameters']['min_committee_member_count']

    cfg = node.database_api.get_config()
    max_committee_count = cfg['GRAPHENE_DEFAULT_MAX_COMMITTEE']

    count = node.get_committee_count()
    assert count >= min_committee_count
    assert count <= max_committee_count


def test_workers_method(local_testnet_no_wallet):
    node = local_testnet_no_wallet.trusted_node

    expected_keys = {
        'id',
        'worker_account',
        'work_begin_date',
        'work_end_date',
        'daily_pay',
        'worker',
        'vote_for',
        'vote_against',
        'total_votes_for',
        'total_votes_against',
        'name',
        'url',
    }

    count = node.get_worker_count()
    workers = node.get_all_workers()

    assert count == len(workers)

    if count > 0:
        for key in expected_keys:
            assert key in workers[0]

    for worker in workers:
        w_acc_id = worker['worker_account']
        w = node.get_workers_by_account([w_acc_id])
        assert worker == w[0]



