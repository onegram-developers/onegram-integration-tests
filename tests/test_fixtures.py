from tests import PrivateTestnet
import pytest


def test_setup(setup):
    assert True


# 'remote_testnet' needs running cli_wallet & witness_node, listening on specified ports
# 'local_testnet' & 'testnet' need just onegram-test image
#  'remote_testnet', 'local_testnet',
@pytest.mark.parametrize('arg', ['testnet'], indirect=True)
def test_join_blockchain(arg):
    net = arg
    assert net.trusted_node.is_alive()
    config = net.trusted_node.get_config()
    print(config)
    assert config
    assert len(config.get('active_witnesses')) == 11


def test_net_with_ico_account(testnet_with_ico_account: PrivateTestnet):
    net = testnet_with_ico_account
    assert not net.cli_wallet.is_locked(), "Wallet should be unlocked"

    accounts = net.cli_wallet.list_my_accounts()

    assert len(accounts) == 1, "There should be exactly one account"

    ico_account = net.accounts[0]

    assert accounts[0]["name"] == ico_account.name, "Ico account should be created!"


def test_net_with_accounts(testnet_with_accounts: PrivateTestnet):
    net = testnet_with_accounts
    assert not net.cli_wallet.is_locked(), "Wallet should be unlocked"

    wallet_accounts = net.cli_wallet.list_my_accounts()
    net_accounts = net.accounts

    assert len(wallet_accounts) == len(net_accounts), "Account count is different"

    wallet_accounts_names = sorted(wallet_accounts, key=lambda account: account["name"])
    net_accounts_names = sorted(net_accounts, key=lambda account: account.name)

    for a, b in zip(wallet_accounts_names, net_accounts_names):
        assert a["name"] == b.name, "Different accounts"


