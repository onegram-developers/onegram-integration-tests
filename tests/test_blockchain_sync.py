from utils import line_processors
from utils import rpc
import os
import sys
from utils import node as _node_
import configparser
import pytest
import docker as _docker


def generate_log_config(config_path):

    default_config = {
        "log.console_appender.stderr": {
            "stream": "std_error"
        },
        "log.file_appender.default": {
            "filename": "logs/default/default.log",
            "rotation_interval": "60",
            "rotation_limit": "7",
        },
        "log.file_appender.p2p": {
            "filename": "logs/p2p/p2p.log",
            "rotation_interval": "60",
            "rotation_limit": "7",
        },
        "log.file_appender.rpc": {
            "filename": "logs/rpc/rpc.log",
            "rotation_interval": "60",
            "rotation_limit": "7",
        },
        "logger.default": {
            "level": "info",
            "appenders": "stderr,default"
        },
        "logger.p2p": {
            "level": "info",
            "appenders": "p2p,stderr"
        },
        "logger.rpc": {
            "level": "error",
            "appenders": "rpc",
        },
    }

    if os.path.isfile(config_path):
        config = configparser.ConfigParser()
        config.read(config_path)
        p2p_default = default_config["logger.p2p"]

        p2p = config["logger.p2p"]
        p2p["level"] = p2p_default["level"]
        p2p["appenders"] += ",stderr"

        with open(config_path, 'w') as config_file:
            config.write(config_file)
    else:
        config = configparser.ConfigParser()
        config.read_dict(default_config)
        with open(config_path, 'w') as config_file:
            config.write(config_file)

# output_dir = "./output"  TODO:
# workspace_directory = "/home/onegram"  TODO:
binary = ""
data_dir = ""
remote = None


@pytest.fixture(scope="session", autouse=True)
def prepare_workspace(request):
    global binary
    global data_dir
    global remote

    options = request.config.option

    binary = options.witness_binary
    data_dir = options.data_dir
    environemnt = options.target_environment
    trusted_rpc_node = "wss://test-rpc.onegramcoin.net/rpc"
    if environemnt and (environemnt == 'MAINNET'):
        trusted_rpc_node = "wss://rpc.onegramcoin.net/rpc"

    remote = rpc.RPC(trusted_rpc_node)

    if data_dir:
        # make sure data dir exists
        os.makedirs(data_dir, exist_ok=True)
        generate_log_config(data_dir + "/logging.ini")


timeout = 1800  # wait 30 min for output
output_filter = line_processors.RegExOutputFilter(r"[0-9]+ms\sth_a\s+")


def check_data_dir_contents_in_docker(data_directory, container):
    if data_directory:
        r = container.exec_run(cmd=f'bash -c "ls -R {data_directory}"', user='root', tty=True)

        print(r.output.decode("utf-8"))
        assert r.exit_code == 0

        r = container.exec_run(cmd=f'bash -c "ls {data_directory}/blockchain/database/block_num_to_block"', user='root', tty=True)

        print(r.output.decode("utf-8"))
        assert r.exit_code == 0


def _test_block_sync(container):
    dyn_props = remote.get_dynamic_global_properties()
    head_block_num = int(dyn_props["head_block_number"])

    node = _node_.Node(binary)

    # Block syncronization test
    processors = [
        line_processors.ExpectingOutputProcessor(timeout),
        line_processors.Write2StreamProcessor(sys.stdout, output_filter),
        line_processors.BlockSyncProcessor(head_block_num),
    ]
    params = ["--resync-blockchain"]
    if data_dir:
        params += ["--data-dir=" + data_dir]

    node.run_session(params, processors, container)


@pytest.mark.skip(reason="for now we will use dockerized version")
def test_block_sync_native():
    _test_block_sync(None)


def test_block_sync_docker(shared_container):
    _test_block_sync(shared_container)


def test_container_content_after_sync(shared_container):
    check_data_dir_contents_in_docker(data_dir, shared_container)


def _test_db_reindex_no_seeds(container):
    # we are expecting that data dir already exists
    assert (not data_dir) or os.path.exists(data_dir)

    node = _node_.Node(binary)

    # Database reindexing test (without seeds)
    processors = [
        line_processors.ExpectingOutputProcessor(timeout),
        line_processors.Write2StreamProcessor(sys.stdout, output_filter),
        line_processors.ReindexingProcessor()
    ]
    params = [
        "--seed-nodes=[]",
        "--replay-blockchain",
    ]
    if data_dir:
        params += ["--data-dir=" + data_dir]

    node.run_session(params, processors, container)


@pytest.mark.skip(reason="for now we will use dockerized version")
def test_db_reindex_no_seeds_native():
    _test_db_reindex_no_seeds(None)


def test_db_reindex_no_seeds_docker(shared_container):
    _test_db_reindex_no_seeds(shared_container)


def test_container_content_after_reindex(shared_container):
    check_data_dir_contents_in_docker(data_dir, shared_container)


def _test_db_reindex_with_seeds(container):
    # we are expecting that data dir already exists
    assert (not data_dir) or os.path.exists(data_dir)

    # Database reindexing test (with seeds)
    dyn_props = remote.get_dynamic_global_properties()
    head_block_num = int(dyn_props["head_block_number"])
    print("head block number: %d" % head_block_num)

    processors = [
        line_processors.ExpectingOutputProcessor(timeout),
        line_processors.Write2StreamProcessor(sys.stdout, output_filter),
        line_processors.ReindexingProcessor(False),
        line_processors.BlockSyncProcessor(head_block_num + 5),
    ]
    params = ["--replay-blockchain"]
    if data_dir:
        params += ["--data-dir=" + data_dir]

    node = _node_.Node(binary)
    node.run_session(params, processors, container)


@pytest.mark.skip(reason="for now we will use dockerized version")
def test_db_reindex_with_seeds_native():
    _test_db_reindex_with_seeds(None)


def test_db_reindex_with_seeds_docker(shared_container):
    _test_db_reindex_with_seeds(shared_container)


def test_container_content_after_reindex_with_seeds(shared_container):
    check_data_dir_contents_in_docker(data_dir, shared_container)


# RUN IT AS python3 -m pytest -s test_blockchain_sync.py

if __name__ == "__main__":
    print("To execute this file as test suite, run command: python3 -m pytest -s test_blockchain_sync.py")

