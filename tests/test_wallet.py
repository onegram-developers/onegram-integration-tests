from tests import PrivateTestnet


def test_wallet_unlock(testnet: PrivateTestnet):
    secret = "supersecret"
    wallet = testnet.cli_wallet

    # set password
    assert wallet.is_new()
    wallet.set_password(secret)
    assert not wallet.is_new()

    # unlock
    assert wallet.is_locked()
    wallet.unlock(secret)

    assert not wallet.is_locked()

