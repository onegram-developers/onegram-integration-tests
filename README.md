## How to run tests

1) Run `docker`
    - Test if `docker` is running `docker info`
    - In some cases you need to *restart* docker
2) Build `onegram-test` image:
    - Unix:
    ```shell
    docker build -t onegram-test --build-arg TARGET_ENV=TESTNET --build-arg BUILD=Release --build-arg REVISION_SHA=`git rev-parse --short HEAD` --build-arg REVISION_TIMESTAMP=`git log -1 --format=%at` -f Dockerfile.OGC .
    ```
    - Windows:
    ```shell
    docker build -t onegram-test --build-arg TARGET_ENV=TESTNET --build-arg BUILD=Release --build-arg REVISION_SHA=$(git rev-parse --short HEAD) --build-arg REVISION_TIMESTAMP=$(git log -1 --format=%at) -f Dockerfile.OGC .
    ```
    > To try `MAINNET` build, change `TARGET_ENV` to `MAINNET`.
3) Install requirements:
    ```shell
    pip install -r requirements.txt
    ```
4) Run tests:
    > Go to the directory where the tests are located (`tests`)

    You can use `run.py` utility to run specific test groups or all tests at once.
    Usage:

    Run all tests silently:
    ```shell
    python run.py --silent
    ```

    Run tests in `common_tests` & `blockchain_tests` groups:
    ```shell
    python run.py common_tests blockchain_tests
    ```

    Alternatively you can run tests directly:

    e.g.
    ```shell
    python -m pytest -s test_account.py
    ```
    or alternatively:
    ```shell
    pytest -s test_account.py
    ```
    
    Running all tests except blockchain sync (this have to run in `onegram` docker image):
    ```shell
    pytest -s --ignore=test_blockchain_sync.py .
    ```
    
    Running specific test from file:
    ```shell
    pytest -s test_fees.py -k test_feeless_account
    ```